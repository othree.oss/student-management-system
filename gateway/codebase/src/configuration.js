const Configuration = Object.freeze({
    ProfessorCommandHandlerQueue: process.env.PROFESSOR_COMMAND_HANDLER_QUEUE,
    ClubCommandHandlerQueue: process.env.CLUB_COMMAND_HANDLER_QUEUE,
    StudentCommandHandlerQueue: process.env.STUDENT_COMMAND_HANDLER_QUEUE,
    ClubMembershipCommandHandlerQueue: process.env.CLUB_MEMBERSHIP_COMMAND_HANDLER_QUEUE,
    RestAPIBaseURL: process.env.REST_API_BASE_URL
})

module.exports = {Configuration}
