const {Try} = require('@othree.io/optional')

function ProfessorClient(fetch, configuration) {
    return Object.freeze({
        getProfessor: async (professorId) => {
            console.log('Getting professor', professorId)
            return Try(async () => {
                const result = await fetch(`${configuration.RestAPIBaseURL}/v1/professors/${professorId}`)
                if (result.ok) {
                    const professor = await result.json()
                    console.log('Found professor', professor)
                    return professor
                }
            })
        }
    })
}

module.exports = {ProfessorClient}
