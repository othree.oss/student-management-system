const {Try} = require('@othree.io/optional')

function StudentClient(fetch, configuration) {
    return Object.freeze({
        getStudent: async (studentId) => {
            console.log('Getting student', studentId)
            return Try(async () => {
                const result = await fetch(`${configuration.RestAPIBaseURL}/v1/students/${studentId}`)
                if (result.ok) {
                    const student = await result.json()
                    console.log('Found student', student)
                    return student
                }
            })
        }
    })
}

module.exports = {StudentClient}
