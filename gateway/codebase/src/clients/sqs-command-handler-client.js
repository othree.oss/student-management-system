const {Optional, Try} = require('@othree.io/optional')

function SqsCommandHandlerClient(sqs, uuid, queueUrl) {
    return Object.freeze({
        send: async (message) => {
            return Try(async () => {
                const params = {
                    MessageAttributes: {
                        'commandType': {
                            DataType: 'String',
                            StringValue: message.type
                        }
                    },
                    MessageGroupId: Optional(message.contextId).orElse(uuid()),
                    MessageBody: JSON.stringify(message),
                    QueueUrl: queueUrl
                }

                console.log('Submitting command to queue', params)
                const queueResponse = await sqs.sendMessage(params).promise()
                console.log('Successfully submitted command to queue', queueResponse)

                return true
            })
        }
    })
}

module.exports = {SqsCommandHandlerClient}
