const {Try} = require('@othree.io/optional')

function ClubMembershipClient(fetch, configuration) {
    return Object.freeze({
        getClubMembership: async (clubId, studentId) => {
            console.log('Getting club membership. Club', clubId, 'Student', studentId)
            return Try(async () => {
                const result = await fetch(`${configuration.RestAPIBaseURL}/v1/clubs/${clubId}/students/${studentId}`)
                if (result.ok) {
                    const clubMembership = await result.json()
                    console.log('Found student', clubMembership)
                    return clubMembership
                }
            })
        }
    })
}

module.exports = {ClubMembershipClient}
