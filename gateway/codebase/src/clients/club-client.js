const {Try} = require('@othree.io/optional')

function ClubClient(fetch, configuration) {
    return Object.freeze({
        getClub: async (clubId) => {
            console.log('Getting club', clubId)
            return Try(async () => {
                const result = await fetch(`${configuration.RestAPIBaseURL}/v1/clubs/${clubId}`)
                if (result.ok) {
                    const club = await result.json()
                    console.log('Found club', club)
                    return club
                }
            })
        }
    })
}

module.exports = {ClubClient}
