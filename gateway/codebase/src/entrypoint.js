const AWS = require('aws-sdk')
const {v4: uuid} = require('uuid')
const {ProfessorService} = require('./services/professor-service')
const {ClubService} = require('./services/club-service')
const {ProfessorClient} = require('./clients/professor-client')
const {StudentService} = require('./services/student-service')
const {ClubClient} = require('./clients/club-client')
const {StudentClient} = require('./clients/student-client')
const {ClubMembershipClient} = require('./clients/club-membership-client')
const {ClubMembershipService} = require('./services/club-membership-service')
const {SqsCommandHandlerClient} = require('./clients/sqs-command-handler-client')
const {ProfessorWsHandlers} = require('./handlers/professor-ws-handlers')
const {StudentWsHandlers} = require('./handlers/student-ws-handlers')
const {ClubWsHandlers} = require('./handlers/club-ws-handlers')
const {ClubMembershipWsHandlers} = require('./handlers/club-membership-ws-handlers')
const {Configuration} = require('./configuration')
const {ResponseHandler} = require('utils').websockets
const fetch = require('node-fetch')

const sqs = new AWS.SQS()
const professorCommandHandler = SqsCommandHandlerClient(sqs, uuid, Configuration.ProfessorCommandHandlerQueue)
const clubCommandHandler = SqsCommandHandlerClient(sqs, uuid, Configuration.ClubCommandHandlerQueue)
const studentCommandHandler = SqsCommandHandlerClient(sqs, uuid, Configuration.StudentCommandHandlerQueue)
const clubMembershipCommandHandler = SqsCommandHandlerClient(sqs, uuid, Configuration.ClubMembershipCommandHandlerQueue)
const professorClient = ProfessorClient(fetch, Configuration)
const clubMembershipClient = ClubMembershipClient(fetch, Configuration)
const clubClient = ClubClient(fetch, Configuration)
const studentClient = StudentClient(fetch, Configuration)
const professorService = ProfessorService(professorCommandHandler, professorClient)
const clubService = ClubService(professorClient, clubCommandHandler, clubClient)
const studentService = StudentService(studentCommandHandler)
const clubMembershipService = ClubMembershipService(clubMembershipCommandHandler, clubMembershipClient, clubClient, studentClient)

const apiGatewayManagementApiBuilder = (endpoint) => {
    return new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: endpoint
    })
}
const responseHandler = ResponseHandler(apiGatewayManagementApiBuilder)

const professorWsHandlers = ProfessorWsHandlers(professorService, responseHandler)
const clubWsHandlers = ClubWsHandlers(clubService, responseHandler)
const studentWsHandlers = StudentWsHandlers(studentService, responseHandler)
const clubMembershipWsHandlers = ClubMembershipWsHandlers(clubMembershipService, responseHandler)

module.exports = {
    ws: {
        professor: {...professorWsHandlers},
        club: {...clubWsHandlers},
        student: {...studentWsHandlers},
        clubMembership: {...clubMembershipWsHandlers}
    }
}
