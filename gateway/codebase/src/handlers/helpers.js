const {ValidationError} = require('utils').validation
const {match} = require('@othree.io/cerillo')

const notifyFailure = async (responseHandler, action, metadata, error) => {
    const maybeNotification = await (match(error)
        .when(_ => _ instanceof ValidationError).then(_ => responseHandler.notifyError(action, metadata, error.errors))
        .default(_ => responseHandler.notifyError(action, metadata, 'Unexpected error'))
        .get())

    if (maybeNotification.isEmpty) {
        console.log('Failed to send error notification', maybeNotification.getError())
    }
}

const extractRequestContext = (event) => {
    const endpoint = `${event.requestContext.domainName}/${event.requestContext.stage}`
    const {connectionId} = event.requestContext

    const request = JSON.parse(event.body)
    const body = request.body
    const transactionId = request.transactionId

    const metadata = {
        endpoint,
        connectionId,
        transactionId
    }

    return {
        request,
        body,
        metadata
    }
}

module.exports = {extractRequestContext, notifyFailure}