const {extractRequestContext, notifyFailure} = require('./helpers')

function ProfessorWsHandlers(professorService, responseHandler) {
    return Object.freeze({
        requestProfessorCreationHandler: async (event) => {
            console.log('Requesting professor creation', event)
            const {request, body: professor, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await professorService.requestProfessorCreation(
                metadata,
                professor
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request professor creation', professor, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        },
        requestPostRatingHandler: async (event) => {
            console.log('Requesting post rating', event)
            const {request, body: rating, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    "transactionId": ["transactionId is required"]
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await professorService.requestPostRating(
                metadata,
                rating
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request post rating', rating, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        }
    })
}

module.exports = {ProfessorWsHandlers}
