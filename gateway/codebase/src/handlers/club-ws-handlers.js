const {extractRequestContext, notifyFailure} = require('./helpers')

function ClubWsHandlers(clubService, responseHandler) {
    return Object.freeze({
        requestClubCreationHandler: async (event) => {
            console.log('Requesting club creation', event)
            const {request, body: club, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await clubService.requestClubCreation(
                metadata,
                club
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request club creation', club, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        },
        requestClubOpeningHandler: async (event) => {
            console.log('Requesting club opening', event)
            const {request, body, metadata} = extractRequestContext(event)
            const {clubId} = body

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            if (!clubId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'clubId': ['clubId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await clubService.requestClubOpening(
                metadata,
                clubId
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request club opening', clubId, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        },
        requestClubClosingHandler: async (event) => {
            console.log('Requesting club closing', event)
            const {request, body, metadata} = extractRequestContext(event)
            const {clubId} = body

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            if (!clubId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'clubId': ['clubId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await clubService.requestClubClosing(
                metadata,
                clubId
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request club closing', clubId, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        }
    })
}

module.exports = {ClubWsHandlers}
