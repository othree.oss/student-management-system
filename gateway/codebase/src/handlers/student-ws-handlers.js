const {extractRequestContext, notifyFailure} = require('./helpers')

function StudentWsHandlers(studentService, responseHandler) {
    return Object.freeze({
        requestStudentCreationHandler: async (event) => {
            console.log('Requesting student creation', event)
            const {request, body: student, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await studentService.requestStudentCreation(
                metadata,
                student
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request student creation', student, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        }
    })
}

module.exports = {StudentWsHandlers}
