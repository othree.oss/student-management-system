const {extractRequestContext, notifyFailure} = require('./helpers')

function ClubMembershipWsHandlers(clubMembershipService, responseHandler) {
    return Object.freeze({
        requestStudentEnrollmentHandler: async (event) => {
            console.log('Requesting student enrollment', event)
            const {request, body: membership, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const maybeResponse = await clubMembershipService.requestStudentEnrollment(
                metadata,
                membership
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request membership creation', membership, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        },
        requestGradedAssignmentCreationHandler: async (event) => {
            console.log('Requesting graded assignment creation', event)
            const {request, body: data, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const membership = {
                studentId: data.studentId,
                clubId: data.clubId
            }

            const gradedAssignment = {
                marksObtained: data.marksObtained,
                totalMarks: data.totalMarks
            }

            const maybeResponse = await clubMembershipService.requestGradedAssignmentCreation(
                metadata,
                membership,
                gradedAssignment
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request graded assignment creation', membership, gradedAssignment, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        },
        requestGradedAssignmentRemovalHandler: async (event) => {
            console.log('Requesting graded assignment creation', event)
            const {request, body: data, metadata} = extractRequestContext(event)

            if (!metadata.transactionId) {
                const maybeNotification = await responseHandler.notifyError(request.action, metadata, {
                    'transactionId': ['transactionId is required']
                })
                if (maybeNotification.isEmpty) {
                    console.log('Failed to send error notification', maybeNotification.getError())
                }
                return {statusCode: 500}
            }

            const membership = {
                studentId: data.studentId,
                clubId: data.clubId
            }

            const assignment = {
                assignmentId: data.assignmentId
            }

            const maybeResponse = await clubMembershipService.requestGradedAssignmentRemoval(
                metadata,
                membership,
                assignment
            )

            if (maybeResponse.isPresent) {
                return {statusCode: 200}
            }

            console.log('Failed to request graded assignment creation', membership, assignment, maybeResponse.getError())
            await notifyFailure(responseHandler, request.action, metadata, maybeResponse.getError())

            return {statusCode: 500}
        }
    })
}

module.exports = {ClubMembershipWsHandlers}
