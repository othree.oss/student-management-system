const CLUB_COMMAND_TYPES = Object.freeze({
    CREATE_CLUB: 'CreateClub',
    OPEN_CLUB: 'OpenClub',
    CLOSE_CLUB: 'CloseClub'
})

module.exports = {CLUB_COMMAND_TYPES}
