const STUDENT_COMMAND_TYPES = Object.freeze({
    CREATE_STUDENT: 'CreateStudent'
})

module.exports = {STUDENT_COMMAND_TYPES}
