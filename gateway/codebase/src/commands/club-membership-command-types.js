const CLUB_MEMBERSHIP_COMMAND_TYPES = Object.freeze({
    ENROLL_STUDENT: 'EnrollStudent',
    ADD_GRADED_ASSIGNMENT: 'AddGradedAssignment',
    REMOVE_GRADED_ASSIGNMENT: 'RemoveGradedAssignment'
})

module.exports = {CLUB_MEMBERSHIP_COMMAND_TYPES}
