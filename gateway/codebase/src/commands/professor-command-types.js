const PROFESSOR_COMMAND_TYPES = Object.freeze({
    CREATE_PROFESSOR: 'CreateProfessor',
    POST_RATING: 'PostRating'
})

module.exports = {PROFESSOR_COMMAND_TYPES}
