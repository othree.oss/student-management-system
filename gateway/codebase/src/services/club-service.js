const {Try} = require('@othree.io/optional')
const {validate, ValidationError} = require('utils').validation
const {ClubConstraints} = require('./validation/club-constraints')
const {CLUB_COMMAND_TYPES} = require('../commands/club-command-types')
const {CLUB_STATUS} = require('./club-status')

function ClubService(professorClient, clubCommandHandler, clubClient) {
    return Object.freeze({
        requestClubCreation: async (metadata, club) => {
            return Try(async () => {
                validate(club, ClubConstraints)

                const maybeProfessor = await professorClient.getProfessor(club.professorId)
                maybeProfessor.orThrow(new ValidationError({
                    professorId: ['Professor not found']
                }))

                const command = {
                    type: CLUB_COMMAND_TYPES.CREATE_CLUB,
                    body: {
                        name: club.name,
                        professorId: club.professorId
                    },
                    metadata: {...metadata}
                }

                return (await clubCommandHandler.send(command)).get()
            })
        },
        requestClubOpening: async (metadata, clubId) => {
            return Try(async () => {
                const maybeClub = await clubClient.getClub(clubId)
                if (maybeClub.isEmpty) {
                    throw new ValidationError({
                        clubId: ['Club not found']
                    })
                }

                if (maybeClub.get().status === CLUB_STATUS.OPENED) {
                    throw new ValidationError({
                        status: [`Club ${clubId} is already opened`]
                    })
                }

                const command = {
                    contextId: clubId,
                    type: CLUB_COMMAND_TYPES.OPEN_CLUB,
                    body: {},
                    metadata: {...metadata}
                }

                return (await clubCommandHandler.send(command)).get()
            })
        },
        requestClubClosing: async (metadata, clubId) => {
            return Try(async () => {
                const maybeClub = await clubClient.getClub(clubId)
                if (maybeClub.isEmpty) {
                    throw new ValidationError({
                        clubId: ['Club not found']
                    })
                }

                if (maybeClub.get().status === CLUB_STATUS.CLOSED) {
                    throw new ValidationError({
                        status: [`Club ${clubId} is already closed`]
                    })
                }

                const command = {
                    contextId: clubId,
                    type: CLUB_COMMAND_TYPES.CLOSE_CLUB,
                    body: {},
                    metadata: {...metadata}
                }

                return (await clubCommandHandler.send(command)).get()
            })
        }
    })
}

module.exports = {ClubService}
