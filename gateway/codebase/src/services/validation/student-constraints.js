const StudentConstraints = Object.freeze({
    name: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    }
})

module.exports = {StudentConstraints}
