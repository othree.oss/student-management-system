const ProfessorConstraints = Object.freeze({
    name: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    }
})

const RatingConstraints = Object.freeze({
    professorId: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    },
    rating: {
        type: 'integer',
        presence: true,
        numericality: {
            greaterThanOrEqualTo: 1,
            lessThanOrEqualTo: 5
        }
    }
})

module.exports = {ProfessorConstraints, RatingConstraints}