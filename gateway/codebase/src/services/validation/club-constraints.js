const ClubConstraints = Object.freeze({
    name: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    },
    professorId: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    }
})

module.exports = {
    ClubConstraints
}