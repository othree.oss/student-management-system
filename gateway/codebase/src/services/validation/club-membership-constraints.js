const ClubMembershipConstraints = Object.freeze({
    clubId: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    },
    studentId: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    }
})

const GradedAssignmentConstraints = Object.freeze({
    marksObtained: (value, attributes) => {
        const maxValue = attributes.totalMarks
        return {
            type: 'number',
            presence: true,
            numericality: {
                greaterThanOrEqualTo: 0,
                lessThanOrEqualTo: maxValue ? maxValue : undefined
            }
        }
    },
    totalMarks: {
        type: 'number',
        presence: true,
        numericality: {
            greaterThan: 0
        }
    }
})

const AssignmentConstraints = Object.freeze({
    assignmentId: {
        type: 'string',
        presence: {
            allowEmpty: false
        }
    }
})

module.exports = {
    ClubMembershipConstraints,
    GradedAssignmentConstraints,
    AssignmentConstraints
}