const {PROFESSOR_COMMAND_TYPES} = require('../commands/professor-command-types')
const {Try} = require('@othree.io/optional')
const {validate, ValidationError} = require('utils').validation
const {ProfessorConstraints, RatingConstraints} = require('./validation/professor-constraints')

function ProfessorService(professorCommandHandler, professorClient) {
    return Object.freeze({
        requestProfessorCreation: async (metadata, professor) => {
            return Try(async () => {
                validate(professor, ProfessorConstraints)

                const command = {
                    type: PROFESSOR_COMMAND_TYPES.CREATE_PROFESSOR,
                    body: {
                        name: professor.name
                    },
                    metadata: {...metadata}
                }

                return (await professorCommandHandler.send(command)).get()
            })
        },
        requestPostRating: async (metadata, rating) => {
            return Try(async () => {
                validate(rating, RatingConstraints)

                const maybeProfessor = await professorClient.getProfessor(rating.professorId)
                maybeProfessor.orThrow(new ValidationError({
                    professorId: ['Professor not found']
                }))

                const command = {
                    contextId: rating.professorId,
                    type: PROFESSOR_COMMAND_TYPES.POST_RATING,
                    body: {
                        rating: rating.rating
                    },
                    metadata: {...metadata}
                }

                return (await professorCommandHandler.send(command)).get()
            })
        }
    })
}

module.exports = {ProfessorService}
