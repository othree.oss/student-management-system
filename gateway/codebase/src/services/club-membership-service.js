const {CLUB_MEMBERSHIP_COMMAND_TYPES} = require('../commands/club-membership-command-types')
const {Try, Optional} = require('@othree.io/optional')
const {validate, ValidationError} = require('utils').validation
const {
    ClubMembershipConstraints,
    GradedAssignmentConstraints,
    AssignmentConstraints
} = require('./validation/club-membership-constraints')

function ClubMembershipService(clubMembershipCommandHandler, clubMembershipClient, clubClient, studentClient) {
    return Object.freeze({
        requestStudentEnrollment: async (metadata, membership) => {
            return Try(async () => {
                validate(membership, ClubMembershipConstraints)

                const eventualClub = clubClient.getClub(membership.clubId)
                    .then(maybeClub => {
                        const club = maybeClub.orThrow(new ValidationError({
                            clubId: [`Club ${membership.clubId} does not exist`]
                        }))

                        if (club.status === 'Closed') {
                            throw new ValidationError({
                                clubId: [`Club ${membership.clubId} is not Opened`]
                            })
                        }
                    })

                const eventualStudent = studentClient.getStudent(membership.studentId)
                    .then(maybeStudent => {
                        return maybeStudent.orThrow(new ValidationError({
                            studentId: [`Student ${membership.studentId} does not exist`]
                        }))
                    })

                const eventualClubMembership = clubMembershipClient.getClubMembership(membership.clubId, membership.studentId)
                    .then(maybeMembership => {
                        if (maybeMembership.isPresent) {
                            throw new ValidationError({
                                membership: [`Student ${membership.studentId} is already enrolled in club ${membership.clubId}`]
                            })
                        }
                    })

                await Promise.all([eventualClub, eventualStudent, eventualClubMembership])

                const command = {
                    type: CLUB_MEMBERSHIP_COMMAND_TYPES.ENROLL_STUDENT,
                    body: {
                        studentId: membership.studentId,
                        clubId: membership.clubId
                    },
                    metadata: {...metadata}
                }

                return (await clubMembershipCommandHandler.send(command)).get()
            })
        },
        requestGradedAssignmentCreation: async (metadata, membership, gradedAssignment) => {
            return Try(async () => {
                validate(membership, ClubMembershipConstraints)
                validate(gradedAssignment, GradedAssignmentConstraints)

                const maybeClubMembership = await clubMembershipClient.getClubMembership(membership.clubId, membership.studentId)
                const clubMembership = maybeClubMembership.orThrow(new ValidationError({
                    membership: [`Student ${membership.studentId} is not enrolled in club ${membership.clubId}`]
                }))

                const command = {
                    contextId: clubMembership.id,
                    type: CLUB_MEMBERSHIP_COMMAND_TYPES.ADD_GRADED_ASSIGNMENT,
                    body: {
                        marksObtained: gradedAssignment.marksObtained,
                        totalMarks: gradedAssignment.totalMarks
                    },
                    metadata: {...metadata}
                }

                return (await clubMembershipCommandHandler.send(command)).get()
            })
        },
        requestGradedAssignmentRemoval: async (metadata, membership, assignment) => {
            return Try(async () => {
                validate(membership, ClubMembershipConstraints)
                validate(assignment, AssignmentConstraints)

                const maybeClubMembership = await clubMembershipClient.getClubMembership(membership.clubId, membership.studentId)
                const clubMembership = maybeClubMembership.orThrow(new ValidationError({
                    membership: [`Student ${membership.studentId} is not enrolled in club ${membership.clubId}`]
                }))

                Optional(clubMembership.assignments.find(_ => _.assignmentId === assignment.assignmentId))
                    .orThrow(new ValidationError({
                        assignmentId: [`Assignment ${assignment.assignmentId} does not exist`]
                    }))

                const command = {
                    contextId: clubMembership.id,
                    type: CLUB_MEMBERSHIP_COMMAND_TYPES.REMOVE_GRADED_ASSIGNMENT,
                    body: {
                        assignmentId: assignment.assignmentId
                    },
                    metadata: {...metadata}
                }

                return (await clubMembershipCommandHandler.send(command)).get()
            })
        }
    })
}

module.exports = {ClubMembershipService}
