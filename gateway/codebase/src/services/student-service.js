const {STUDENT_COMMAND_TYPES} = require('../commands/student-command-types')
const {Try} = require('@othree.io/optional')
const {validate} = require('utils').validation
const {StudentConstraints} = require('./validation/student-constraints')

function StudentService(studentCommandHandler) {
    return Object.freeze({
        requestStudentCreation: async (metadata, student) => {
            return Try(async () => {
                validate(student, StudentConstraints)

                const command = {
                    type: STUDENT_COMMAND_TYPES.CREATE_STUDENT,
                    body: {
                        name: student.name
                    },
                    metadata: {...metadata}
                }

                return (await studentCommandHandler.send(command)).get()
            })
        }
    })
}

module.exports = {StudentService}
