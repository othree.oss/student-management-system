const CLUB_STATUS = Object.freeze({
    CLOSED: 'Closed',
    OPENED: 'Opened'
})

module.exports = {CLUB_STATUS}
