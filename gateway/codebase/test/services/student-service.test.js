const {StudentService} = require('../../src/services/student-service')
const {STUDENT_COMMAND_TYPES} = require('../../src/commands/student-command-types')
const {Optional} = require('@othree.io/optional')

describe('StudentService', () => {
    describe('requestProfessorCreation', () => {
        it('should send the CreateProfessor command request', async () => {
            const studentCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        type:STUDENT_COMMAND_TYPES.CREATE_STUDENT,
                        body: {
                            name: 'Bruce Lee'
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }
            const service = StudentService(studentCommandHandler)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const student = {
                name: 'Bruce Lee'
            }

            const maybeResponse = await service.requestStudentCreation(metadata, student)

            expect(maybeResponse.get()).toBeTruthy()
            expect(maybeResponse.isPresent).toBeTruthy()
        })
    })
})
