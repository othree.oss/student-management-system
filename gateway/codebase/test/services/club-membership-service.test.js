const {ClubMembershipService} = require('../../src/services/club-membership-service')
const {CLUB_MEMBERSHIP_COMMAND_TYPES} = require('../../src/commands/club-membership-command-types')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation

describe('ClubMembershipService', () => {
    describe('requestStudentEnrollment', () => {
        it('should send the EnrollStudent command request', async () => {
            const clubMembershipCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        type: CLUB_MEMBERSHIP_COMMAND_TYPES.ENROLL_STUDENT,
                        body: {
                            studentId: '123',
                            clubId: 'abc'
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    expect(clubId).toEqual('abc')
                    expect(studentId).toEqual('123')

                    return Optional()
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    expect(clubId).toEqual('abc')

                    return Optional({
                        id: 'abc',
                        name: 'Chess',
                        professorId: '007',
                        status: 'Opened'
                    })
                }
            }

            const studentClient = {
                getStudent: async (studentId) => {
                    expect(studentId).toEqual('123')

                    return Optional({
                        id: '123',
                        name: 'Bruce Lee'
                    })
                }
            }

            const service = ClubMembershipService(clubMembershipCommandHandler, clubMembershipClient, clubClient, studentClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const maybeResponse = await service.requestStudentEnrollment(metadata, membership)
            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should throw a validation error if the student is already enrolled in the specified club', async () => {
            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional({
                        id: '1337',
                        studentId: '123',
                        clubId: 'abc',
                        status: false,
                        assignments: [],
                        grade: 0
                    })
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    return Optional({
                        id: 'abc',
                        name: 'Chess',
                        professorId: '007',
                        status: 'Opened'
                    })
                }
            }

            const studentClient = {
                getStudent: async (studentId) => {
                    return Optional({
                        id: '123',
                        name: 'Bruce Lee'
                    })
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, clubClient, studentClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const maybeResponse = await service.requestStudentEnrollment(metadata, membership)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                membership: ['Student 123 is already enrolled in club abc']
            })
        })

        it('should throw a validation error if the club is not Opened', async () => {
            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional()
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    return Optional({
                        id: 'abc',
                        name: 'Chess',
                        professorId: '007',
                        status: 'Closed'
                    })
                }
            }

            const studentClient = {
                getStudent: async (studentId) => {
                    return Optional({
                        id: '123',
                        name: 'Bruce Lee'
                    })
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, clubClient, studentClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const maybeResponse = await service.requestStudentEnrollment(metadata, membership)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                clubId: ['Club abc is not Opened']
            })
        })

        it('should throw a validation error if the student does not exist', async () => {
            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional()
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    return Optional({
                        id: 'abc',
                        name: 'Chess',
                        professorId: '007',
                        status: 'Opened'
                    })
                }
            }

            const studentClient = {
                getStudent: async (studentId) => {
                    return Optional()
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, clubClient, studentClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const maybeResponse = await service.requestStudentEnrollment(metadata, membership)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                studentId: ['Student 123 does not exist']
            })
        })

        it('should throw a validation error if the club does not exist', async () => {
            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional()
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    return Optional()
                }
            }

            const studentClient = {
                getStudent: async (studentId) => {
                    return Optional({
                        id: '123',
                        name: 'Bruce Lee'
                    })
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, clubClient, studentClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const maybeResponse = await service.requestStudentEnrollment(metadata, membership)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                clubId: ['Club abc does not exist']
            })
        })
    })

    describe('requestGradedAssignmentCreation', () => {
        it('should send the AddGradedAssignment command request', async () => {
            const clubMembershipCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        contextId: '1337',
                        type: CLUB_MEMBERSHIP_COMMAND_TYPES.ADD_GRADED_ASSIGNMENT,
                        body: {
                            marksObtained: 5,
                            totalMarks: 10
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    expect(clubId).toEqual('abc')
                    expect(studentId).toEqual('123')

                    return Optional({
                        id: '1337',
                        studentId: '123',
                        clubId: 'abc',
                        status: false,
                        assignments: [],
                        grade: 0
                    })
                }
            }

            const service = ClubMembershipService(clubMembershipCommandHandler, clubMembershipClient, {}, {})

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const gradedAssignment = {
                marksObtained: 5,
                totalMarks: 10
            }

            const maybeResponse = await service.requestGradedAssignmentCreation(metadata, membership, gradedAssignment)
            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should throw a validation error if the membership does not exist', async () => {

            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional()
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, {}, {})

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const gradedAssignment = {
                marksObtained: 5,
                totalMarks: 10
            }

            const maybeResponse = await service.requestGradedAssignmentCreation(metadata, membership, gradedAssignment)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                membership: ['Student 123 is not enrolled in club abc']
            })
        })
    })

    describe('requestGradedAssignmentRemoval', () => {
        it('should send the RemoveGradedAssignment command request', async () => {
            const clubMembershipCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        contextId: '1337',
                        type: CLUB_MEMBERSHIP_COMMAND_TYPES.REMOVE_GRADED_ASSIGNMENT,
                        body: {
                            assignmentId: 'phys1c5'
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    expect(clubId).toEqual('abc')
                    expect(studentId).toEqual('123')

                    return Optional({
                        id: '1337',
                        studentId: '123',
                        clubId: 'abc',
                        status: false,
                        assignments: [{
                            assignmentId: 'phys1c5',
                            marksObtained: 7,
                            totalMarks: 10,
                            grade: 70
                        }],
                        grade: 0
                    })
                }
            }

            const service = ClubMembershipService(clubMembershipCommandHandler, clubMembershipClient, {}, {})

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const assignment = {
                assignmentId: 'phys1c5'
            }

            const maybeResponse = await service.requestGradedAssignmentRemoval(metadata, membership, assignment)
            expect(maybeResponse.get()).toBeTruthy()
            expect(maybeResponse.isPresent).toBeTruthy()
        })

        it('should throw a validation error if the assignment does not exist', async () => {
            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional({
                        id: '1337',
                        studentId: '123',
                        clubId: 'abc',
                        status: false,
                        assignments: [],
                        grade: 0
                    })
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, {}, {})

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const assignment = {
                assignmentId: 'phys1c5'
            }

            const maybeResponse = await service.requestGradedAssignmentRemoval(metadata, membership, assignment)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                assignmentId: ['Assignment phys1c5 does not exist']
            })
        })

        it('should throw a validation error if the membership does not exist', async () => {
            const clubMembershipClient = {
                getClubMembership: async (clubId, studentId) => {
                    return Optional()
                }
            }

            const service = ClubMembershipService({}, clubMembershipClient, {}, {})

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const membership = {
                clubId: 'abc',
                studentId: '123'
            }

            const assignment = {
                assignmentId: 'phys1c5'
            }

            const maybeResponse = await service.requestGradedAssignmentRemoval(metadata, membership, assignment)
            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                membership: ['Student 123 is not enrolled in club abc']
            })
        })
    })
})
