const {ProfessorService} = require('../../src/services/professor-service')
const {PROFESSOR_COMMAND_TYPES} = require('../../src/commands/professor-command-types')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation

describe('ProfessorService', () => {
    describe('requestProfessorCreation', () => {
        it('should send the CreateProfessor command request', async () => {
            const professorCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        type:PROFESSOR_COMMAND_TYPES.CREATE_PROFESSOR,
                        body: {
                            name: 'Chuck Norris'
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }
            const service = ProfessorService(professorCommandHandler)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const professor = {
                name: 'Chuck Norris'
            }

            const maybeResponse = await service.requestProfessorCreation(metadata, professor)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })
    })

    describe('requestPostRating', () => {
        it('should send the PostRating command request', async () => {
            const professorCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        contextId: '1337',
                        type:PROFESSOR_COMMAND_TYPES.POST_RATING,
                        body: {
                            rating: 5
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const professorClient = {
                getProfessor: async (professorId) => {
                    expect(professorId).toEqual('1337')

                    return Optional({
                        id: '1337',
                        name: 'Chuck Norris',
                        rating: {
                            overallRating: 0,
                            stars: [0, 0, 0, 0, 0]
                        }
                    })
                }
            }
            const service = ProfessorService(professorCommandHandler, professorClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const rating = {
                professorId: '1337',
                rating: 5
            }

            const maybeResponse = await service.requestPostRating(metadata, rating)

            expect(maybeResponse.get()).toBeTruthy()
            expect(maybeResponse.isPresent).toBeTruthy()
        })

        it('should throw a validation error if the professor is not found', async () => {

            const professorClient = {
                getProfessor: async (professorId) => {
                    return Optional()
                }
            }
            const service = ProfessorService({}, professorClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const rating = {
                professorId: '1337',
                rating: 5
            }

            const maybeResponse = await service.requestPostRating(metadata, rating)

            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError()).toStrictEqual(new ValidationError({
                professorId: ['Professor not found']
            }))
        })
    })
})
