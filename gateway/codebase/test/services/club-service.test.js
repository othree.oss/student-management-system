const {ClubService} = require('../../src/services/club-service')
const {CLUB_COMMAND_TYPES} = require('../../src/commands/club-command-types')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation
const {CLUB_STATUS} = require('../../src/services/club-status')

describe('ClubService', () => {
    describe('requestClubCreation', () => {
        it('should send the CreateClub command request', async () => {
            const clubCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        type: CLUB_COMMAND_TYPES.CREATE_CLUB,
                        body: {
                            name: 'Chess',
                            professorId: '1337'
                        },
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const professorClient = {
                getProfessor: async (professorId) => {
                    expect(professorId).toEqual('1337')

                    return Optional({
                        id: '1337',
                        name: 'Chuck Norris',
                        rating: {
                            overallRating: 0,
                            stars: [0, 0, 0, 0, 0]
                        }
                    })
                }
            }

            const service = ClubService(professorClient, clubCommandHandler)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const club = {
                name: 'Chess',
                professorId: '1337'
            }

            const maybeResponse = await service.requestClubCreation(metadata, club)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should throw a validation error if the professor is not found', async () => {
            const professorClient = {
                getProfessor: async (professorId) => {
                    return Optional()
                }
            }

            const service = ClubService(professorClient, {})

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const club = {
                name: 'Chess',
                professorId: '1337'
            }

            const maybeResponse = await service.requestClubCreation(metadata, club)

            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                professorId: ['Professor not found']
            })
        })
    })

    describe('requestClubOpening', () => {
        it('should send the OpenClub command request', async () => {
            const clubCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        contextId: '1337',
                        type: CLUB_COMMAND_TYPES.OPEN_CLUB,
                        body: {},
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    expect(clubId).toEqual('1337')

                    return Optional({
                        id: '1337',
                        name: 'Chess',
                        professorId: '007',
                        status: CLUB_STATUS.CLOSED
                    })
                }
            }

            const service = ClubService({}, clubCommandHandler, clubClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const maybeResponse = await service.requestClubOpening(metadata, '1337')

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should throw a validation error if the club is not found', async () => {
            const clubClient = {
                getClub: async (clubId) => {
                    return Optional()
                }
            }

            const service = ClubService({}, {}, clubClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const maybeResponse = await service.requestClubOpening(metadata, '1337')

            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                clubId: ['Club not found']
            })
        })

        it('should throw a validation error if the club is already opened', async () => {
            const clubClient = {
                getClub: async (clubId) => {
                    return Optional({
                        id: '1337',
                        name: 'Chess',
                        professorId: '007',
                        status: CLUB_STATUS.OPENED
                    })
                }
            }

            const service = ClubService({}, {}, clubClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const maybeResponse = await service.requestClubOpening(metadata, '1337')

            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                status: ['Club 1337 is already opened']
            })
        })
    })

    describe('requestClubClosing', () => {
        it('should send the CloseClub command request', async () => {
            const clubCommandHandler = {
                send: async (command) => {
                    expect(command).toStrictEqual({
                        contextId: '1337',
                        type: CLUB_COMMAND_TYPES.CLOSE_CLUB,
                        body: {},
                        metadata: {
                            connectionId: 'c0nn3ct10n',
                            wsEndpoint: 'ws://localhost'
                        }
                    })

                    return Optional(true)
                }
            }

            const clubClient = {
                getClub: async (clubId) => {
                    expect(clubId).toEqual('1337')

                    return Optional({
                        id: '1337',
                        name: 'Chess',
                        professorId: '007',
                        status: CLUB_STATUS.OPENED
                    })
                }
            }

            const service = ClubService({}, clubCommandHandler, clubClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const maybeResponse = await service.requestClubClosing(metadata, '1337')

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should throw a validation error if the club is not found', async () => {
            const clubClient = {
                getClub: async (clubId) => {
                    return Optional()
                }
            }

            const service = ClubService({}, {}, clubClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const maybeResponse = await service.requestClubClosing(metadata, '1337')

            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                clubId: ['Club not found']
            })
        })

        it('should throw a validation error if the club is already closed', async () => {
            const clubClient = {
                getClub: async (clubId) => {
                    return Optional({
                        id: '1337',
                        name: 'Chess',
                        professorId: '007',
                        status: CLUB_STATUS.CLOSED
                    })
                }
            }

            const service = ClubService({}, {}, clubClient)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                wsEndpoint: 'ws://localhost'
            }

            const maybeResponse = await service.requestClubClosing(metadata, '1337')

            expect(maybeResponse.isPresent).toBeFalsy()
            expect(maybeResponse.getError() instanceof ValidationError).toBeTruthy()
            expect(maybeResponse.getError().errors).toStrictEqual({
                status: ['Club 1337 is already closed']
            })
        })
    })
})
