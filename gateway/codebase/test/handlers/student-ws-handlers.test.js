const {StudentWsHandlers} = require('../../src/handlers/student-ws-handlers')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation

describe('StudentWsHandlers', () => {
    describe('requestStudentCreationHandler', () => {
        it('should return 200 if successful', async () => {
            const studentService = {
                requestStudentCreation: async (metadata, student) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(student).toStrictEqual({
                        name: 'Bruce Lee'
                    })

                    return Optional(true)
                }
            }
            const handler = StudentWsHandlers(studentService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createstudent',
                    body: {
                        name: 'Bruce Lee'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestStudentCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('createstudent')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = StudentWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createstudent',
                    body: {
                        name: 'Bruce Lee'
                    }
                })
            }

            const response = await handler.requestStudentCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const studentService = {
                requestStudentCreation: async (metadata, student) => {
                    return Optional(undefined, new ValidationError({name: ['name is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('createstudent')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        name: ['name is required']
                    })

                    return Optional(true)
                }
            }

            const handler = StudentWsHandlers(studentService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createstudent',
                    body: {
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestStudentCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })
})
