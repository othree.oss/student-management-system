const {ProfessorWsHandlers} = require('../../src/handlers/professor-ws-handlers')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation

describe('ProfessorWsHandlers', () => {
    describe('requestProfessorCreationHandler', () => {
        it('should return 200 if successful', async () => {
            const professorService = {
                requestProfessorCreation: async (metadata, professor) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(professor).toStrictEqual({
                        name: 'Chuck Norris'
                    })

                    return Optional(true)
                }
            }
            const handler = ProfessorWsHandlers(professorService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createprofessor',
                    body: {
                        name: 'Chuck Norris'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestProfessorCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('createprofessor')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ProfessorWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createprofessor',
                    body: {
                        name: 'Chuck Norris'
                    }
                })
            }

            const response = await handler.requestProfessorCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const professorService = {
                requestProfessorCreation: async (metadata, professor) => {
                    return Optional(undefined, new ValidationError({name: ['name is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('createprofessor')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        name: ['name is required']
                    })

                    return Optional(true)
                }
            }

            const handler = ProfessorWsHandlers(professorService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createprofessor',
                    body: {
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestProfessorCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })

    describe('requestPostRatingHandler', () => {
        it('should return 200 if successful', async () => {
            const professorService = {
                requestPostRating: async (metadata, rating) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(rating).toStrictEqual({
                        professorId: '1337',
                        rating: 5
                    })

                    return Optional(true)
                }
            }
            const handler = ProfessorWsHandlers(professorService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'postrating',
                    body: {
                        professorId: '1337',
                        rating: 5
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestPostRatingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('postrating')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ProfessorWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'postrating',
                    body: {
                        professorId: '1337',
                        rating: 5
                    }
                })
            }

            const response = await handler.requestPostRatingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const professorService = {
                requestPostRating: async (metadata, rating) => {
                    return Optional(undefined, new ValidationError({rating: ['rating is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('postrating')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        rating: ['rating is required']
                    })

                    return Optional(true)
                }
            }

            const handler = ProfessorWsHandlers(professorService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'postrating',
                    body: {
                        professorId: '1337'
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestPostRatingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })
})
