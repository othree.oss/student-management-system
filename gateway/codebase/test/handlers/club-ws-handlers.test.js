const {ClubWsHandlers} = require('../../src/handlers/club-ws-handlers')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation

describe('ClubWsHandlers', () => {
    describe('requestClubCreationHandler', () => {
        it('should return 200 if successful', async () => {
            const clubService = {
                requestClubCreation: async (metadata, club) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(club).toStrictEqual({
                        name: 'Chess',
                        professorId: '1337'
                    })

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers(clubService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createclub',
                    body: {
                        name: 'Chess',
                        professorId: '1337'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestClubCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('createclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createclub',
                    body: {
                        name: 'Chess',
                        professorId: '1337'
                    }
                })
            }

            const response = await handler.requestClubCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const clubService = {
                requestClubCreation: async (metadata, club) => {
                    return Optional(undefined, new ValidationError({name: ['name is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('createclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        name: ['name is required']
                    })

                    return Optional(true)
                }
            }

            const handler = ClubWsHandlers(clubService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'createclub',
                    body: {
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestClubCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })

    describe('requestClubOpeningHandler', () => {
        it('should return 200 if successful', async () => {
            const clubService = {
                requestClubOpening: async (metadata, clubId) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(clubId).toStrictEqual('1337')

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers(clubService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'openclub',
                    body: {
                        clubId: '1337'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestClubOpeningHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if the clubId is not provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('openclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(errors).toStrictEqual({
                        clubId: ['clubId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'openclub',
                    body: {},
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestClubOpeningHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('openclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'openclub',
                    body: {
                        clubId: '1337'
                    }
                })
            }

            const response = await handler.requestClubOpeningHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const clubService = {
                requestClubOpening: async (metadata, clubId) => {
                    return Optional(undefined, new ValidationError({clubId: ['Club not found']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('openclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        clubId: ['Club not found']
                    })

                    return Optional(true)
                }
            }

            const handler = ClubWsHandlers(clubService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'openclub',
                    body: {
                        clubId: '1337'
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestClubOpeningHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })

    describe('requestClubClosingHandler', () => {
        it('should return 200 if successful', async () => {
            const clubService = {
                requestClubClosing: async (metadata, clubId) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(clubId).toStrictEqual('1337')

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers(clubService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'closeclub',
                    body: {
                        clubId: '1337'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestClubClosingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if the clubId is not provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('closeclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(errors).toStrictEqual({
                        clubId: ['clubId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'closeclub',
                    body: {},
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestClubClosingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('closeclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'closeclub',
                    body: {
                        clubId: '1337'
                    }
                })
            }

            const response = await handler.requestClubClosingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const clubService = {
                requestClubClosing: async (metadata, clubId) => {
                    return Optional(undefined, new ValidationError({clubId: ['Club not found']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('closeclub')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        clubId: ['Club not found']
                    })

                    return Optional(true)
                }
            }

            const handler = ClubWsHandlers(clubService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'closeclub',
                    body: {
                        clubId: '1337'
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestClubClosingHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })
})
