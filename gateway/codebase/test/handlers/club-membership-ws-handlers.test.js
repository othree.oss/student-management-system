const {ClubMembershipWsHandlers} = require('../../src/handlers/club-membership-ws-handlers')
const {Optional} = require('@othree.io/optional')
const {ValidationError} = require('utils').validation

describe('ClubMembershipWsHandlers', () => {
    describe('requestClubCreationHandler', () => {
        it('should return 200 if successful', async () => {
            const clubMembershipService = {
                requestStudentEnrollment: async (metadata, membership) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(membership).toStrictEqual({
                        studentId: 'abc',
                        clubId: '123'
                    })

                    return Optional(true)
                }
            }
            const handler = ClubMembershipWsHandlers(clubMembershipService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'enrollstudent',
                    body: {
                        studentId: 'abc',
                        clubId: '123'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestStudentEnrollmentHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('enrollstudent')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubMembershipWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'enrollstudent',
                    body: {
                        studentId: 'abc',
                        clubId: '123'
                    }
                })
            }

            const response = await handler.requestStudentEnrollmentHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const clubMembershipService = {
                requestStudentEnrollment: async (metadata, membership) => {
                    return Optional(undefined, new ValidationError({studentId: ['studentId is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('enrollstudent')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        studentId: ['studentId is required']
                    })

                    return Optional(true)
                }
            }

            const handler = ClubMembershipWsHandlers(clubMembershipService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'enrollstudent',
                    body: {
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestStudentEnrollmentHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })

    describe('requestGradedAssignmentCreationHandler', () => {
        it('should return 200 if successful', async () => {
            const clubMembershipService = {
                requestGradedAssignmentCreation: async (metadata, membership, gradedAssignment) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(membership).toStrictEqual({
                        studentId: 'abc',
                        clubId: '123'
                    })
                    expect(gradedAssignment).toStrictEqual({
                        marksObtained: 5,
                        totalMarks: 10
                    })

                    return Optional(true)
                }
            }
            const handler = ClubMembershipWsHandlers(clubMembershipService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'addgradedassignment',
                    body: {
                        studentId: 'abc',
                        clubId: '123',
                        marksObtained: 5,
                        totalMarks: 10
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestGradedAssignmentCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('addgradedassignment')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubMembershipWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'addgradedassignment',
                    body: {
                        studentId: 'abc',
                        clubId: '123',
                        marksObtained: 5,
                        totalMarks: 10
                    }
                })
            }

            const response = await handler.requestGradedAssignmentCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const clubMembershipService = {
                requestGradedAssignmentCreation: async (metadata, membership, gradedAssignment) => {
                    return Optional(undefined, new ValidationError({studentId: ['studentId is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('addgradedassignment')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        studentId: ['studentId is required']
                    })

                    return Optional(true)
                }
            }

            const handler = ClubMembershipWsHandlers(clubMembershipService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'addgradedassignment',
                    body: {
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestGradedAssignmentCreationHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })

    describe('requestAssignmentRemovalHandler', () => {
        it('should return 200 if successful', async () => {
            const clubMembershipService = {
                requestGradedAssignmentRemoval: async (metadata, membership, assignment) => {
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4ns4ct10n'
                    })
                    expect(membership).toStrictEqual({
                        studentId: 'abc',
                        clubId: '123'
                    })
                    expect(assignment).toStrictEqual({
                        assignmentId: 'phys1c5'
                    })

                    return Optional(true)
                }
            }
            const handler = ClubMembershipWsHandlers(clubMembershipService)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'removegradedassignment',
                    body: {
                        studentId: 'abc',
                        clubId: '123',
                        assignmentId: 'phys1c5'
                    },
                    transactionId: '7r4ns4ct10n'
                })
            }

            const response = await handler.requestGradedAssignmentRemovalHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 200
            })
        })

        it('should notify an error and return 500 if no transaction id is provided', async () => {
            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('removegradedassignment')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: undefined
                    })
                    expect(errors).toStrictEqual({
                        transactionId: ['transactionId is required']
                    })

                    return Optional(true)
                }
            }
            const handler = ClubMembershipWsHandlers({}, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'removegradedassignment',
                    body: {
                        studentId: 'abc',
                        clubId: '123',
                        assignmentId: 'phys1c5'
                    }
                })
            }

            const response = await handler.requestGradedAssignmentRemovalHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })

        it('should return 500 if it fails', async () => {
            const clubMembershipService = {
                requestGradedAssignmentRemoval: async (metadata, membership, assignment) => {
                    return Optional(undefined, new ValidationError({studentId: ['studentId is required']}))
                }
            }

            const responseHandler = {
                notifyError: async (action, metadata, errors) => {
                    expect(action).toEqual('removegradedassignment')
                    expect(metadata).toStrictEqual({
                        endpoint: 'ws://localhost/prod',
                        connectionId: 'c0nn3ct10n',
                        transactionId: '7r4n54c710n'
                    })
                    expect(errors).toStrictEqual({
                        studentId: ['studentId is required']
                    })

                    return Optional(true)
                }
            }

            const handler = ClubMembershipWsHandlers(clubMembershipService, responseHandler)

            const lambdaEvent = {
                requestContext: {
                    domainName: 'ws://localhost',
                    stage: 'prod',
                    connectionId: 'c0nn3ct10n'
                },
                body: JSON.stringify({
                    action: 'removegradedassignment',
                    body: {
                    },
                    transactionId: '7r4n54c710n'
                })
            }

            const response = await handler.requestGradedAssignmentRemovalHandler(lambdaEvent)

            expect(response).toStrictEqual({
                statusCode: 500
            })
        })
    })
})
