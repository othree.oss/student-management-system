const {ClubMembershipClient} = require('../../src/clients/club-membership-client')

describe('ClubMembershipClient', () => {
    const configuration = {
        RestAPIBaseURL: 'http://localhost'
    }
    describe('getClubMembership', () => {
        it('should return the requested membership', async () => {
            const fetch = async (url) => {
                expect(url).toEqual('http://localhost/v1/clubs/123/students/abc')

                return {
                    ok: true,
                    json: async () => {
                        return {
                            id: '1337',
                            studentId: 'abc',
                            clubId: '123',
                            status: false,
                            assignments: [],
                            grade: 0
                        }
                    }
                }
            }
            const client = ClubMembershipClient(fetch, configuration)

            const maybeMembership = await client.getClubMembership('123', 'abc')
            expect(maybeMembership.isPresent).toBeTruthy()
            expect(maybeMembership.get()).toStrictEqual({
                id: '1337',
                studentId: 'abc',
                clubId: '123',
                status: false,
                assignments: [],
                grade: 0
            })
        })

        it('should return an empty optional if the membership is not found', async () => {
            const fetch = async (url) => {
                return {
                    ok: false
                }
            }
            const client = ClubMembershipClient(fetch, configuration)

            const maybeMembership = await client.getClubMembership('123', 'abc')
            expect(maybeMembership.isPresent).toBeFalsy()
        })
    })
})