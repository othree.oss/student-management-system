const {ProfessorClient} = require('../../src/clients/professor-client')

describe('ProfessorClient', () => {
    const configuration = {
        RestAPIBaseURL: 'http://localhost'
    }
    describe('getProfessor', () => {
        it('should return the requested professor', async () => {
            const fetch = async (url) => {
                expect(url).toEqual('http://localhost/v1/professors/1337')

                return {
                    ok: true,
                    json: async () => {
                        return {
                            id: '1337',
                            name: 'Chuck Norris',
                            rating: {
                                overallRating: 0,
                                stars: [0, 0, 0, 0, 0]
                            }
                        }
                    }
                }
            }
            const client = ProfessorClient(fetch, configuration)

            const maybeProfessor = await client.getProfessor('1337')
            expect(maybeProfessor.isPresent).toBeTruthy()
            expect(maybeProfessor.get()).toStrictEqual({
                id: '1337',
                name: 'Chuck Norris',
                rating: {
                    overallRating: 0,
                    stars: [0, 0, 0, 0, 0]
                }
            })
        })

        it('should return an empty optional if the professor is not found', async () => {
            const fetch = async (url) => {
                return {
                    ok: false
                }
            }
            const client = ProfessorClient(fetch, configuration)

            const maybeProfessor = await client.getProfessor('1337')
            expect(maybeProfessor.isPresent).toBeFalsy()
        })
    })
})