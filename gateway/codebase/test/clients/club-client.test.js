const {ClubClient} = require('../../src/clients/club-client')

describe('ClubClient', () => {
    const configuration = {
        RestAPIBaseURL: 'http://localhost'
    }
    describe('getClub', () => {
        it('should return the requested club', async () => {
            const fetch = async (url) => {
                expect(url).toEqual('http://localhost/v1/clubs/1337')

                return {
                    ok: true,
                    json: async () => {
                        return {
                            id: '1337',
                            name: 'Chess',
                            professorId: '007',
                            status: 'Opened'
                        }
                    }
                }
            }
            const client = ClubClient(fetch, configuration)

            const maybeClub = await client.getClub('1337')
            expect(maybeClub.isPresent).toBeTruthy()
            expect(maybeClub.get()).toStrictEqual({
                id: '1337',
                name: 'Chess',
                professorId: '007',
                status: 'Opened'
            })
        })

        it('should return an empty optional if the professor is not found', async () => {
            const fetch = async (url) => {
                return {
                    ok: false
                }
            }
            const client = ClubClient(fetch, configuration)

            const maybeClub = await client.getClub('1337')
            expect(maybeClub.isPresent).toBeFalsy()
        })
    })
})