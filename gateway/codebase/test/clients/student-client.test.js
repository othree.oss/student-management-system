const {StudentClient} = require('../../src/clients/student-client')

describe('StudentClient', () => {
    const configuration = {
        RestAPIBaseURL: 'http://localhost'
    }
    describe('getStudent', () => {
        it('should return the requested student', async () => {
            const fetch = async (url) => {
                expect(url).toEqual('http://localhost/v1/students/7331')

                return {
                    ok: true,
                    json: async () => {
                        return {
                            id: '7331',
                            name: 'Bruce Lee'
                        }
                    }
                }
            }
            const client = StudentClient(fetch, configuration)

            const maybeStudent = await client.getStudent('7331')
            expect(maybeStudent.isPresent).toBeTruthy()
            expect(maybeStudent.get()).toStrictEqual({
                id: '7331',
                name: 'Bruce Lee'
            })
        })

        it('should return an empty optional if the student is not found', async () => {
            const fetch = async (url) => {
                return {
                    ok: false
                }
            }
            const client = StudentClient(fetch, configuration)

            const maybeStudent = await client.getStudent('7331')
            expect(maybeStudent.isPresent).toBeFalsy()
        })
    })
})