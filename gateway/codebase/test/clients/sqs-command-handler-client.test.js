const {SqsCommandHandlerClient} = require('../../src/clients/sqs-command-handler-client')

describe('SqsCommandHandlerClient', () => {
    describe('send', () => {
        const uuid = () => '1337'
        const queueUrl = 'https://professor.queue.sqs'

        it('should send the specified message with a new message group id', async () => {
            const sqs = {
                sendMessage: (params) => {
                    expect(params).toStrictEqual({
                        MessageAttributes: {
                            'commandType': {
                                DataType: 'String',
                                StringValue: message.type
                            }
                        },
                        MessageGroupId: '1337',
                        MessageBody: JSON.stringify({
                            type: 'COMMAND',
                            body: {
                                message: 'Hello World'
                            }
                        }),
                        QueueUrl: queueUrl
                    })

                    return {
                        promise: async () => {
                            return true
                        }
                    }
                }
            }
            const sqsClient = SqsCommandHandlerClient(sqs, uuid, queueUrl)

            const message = {
                type: 'COMMAND',
                body: {
                    message: 'Hello World'
                }
            }

            const maybeSent = await sqsClient.send(message)

            expect(maybeSent.isPresent).toBeTruthy()
            expect(maybeSent.get()).toBeTruthy()

        })

        it('should send the specified message with the provided contextId as message group id', async () => {
            const sqs = {
                sendMessage: (params) => {
                    expect(params).toStrictEqual({
                        MessageAttributes: {
                            'commandType': {
                                DataType: 'String',
                                StringValue: message.type
                            }
                        },
                        MessageGroupId: '123abc',
                        MessageBody: JSON.stringify({
                            contextId: '123abc',
                            type: 'COMMAND',
                            body: {
                                message: 'Hello World'
                            }
                        }),
                        QueueUrl: queueUrl
                    })

                    return {
                        promise: async () => {
                            return true
                        }
                    }
                }
            }
            const sqsClient = SqsCommandHandlerClient(sqs, uuid, queueUrl)

            const message = {
                contextId: '123abc',
                type: 'COMMAND',
                body: {
                    message: 'Hello World'
                }
            }

            const maybeSent = await sqsClient.send(message)

            expect(maybeSent.isPresent).toBeTruthy()
            expect(maybeSent.get()).toBeTruthy()

        })
    })
})
