describe('Configuration', () => {
    process.env.PROFESSOR_COMMAND_HANDLER_QUEUE = 'aws:arn:ProfessorCommandHandlerQueue'
    process.env.CLUB_COMMAND_HANDLER_QUEUE = 'aws:arn:ClubCommandHandlerQueue'
    process.env.STUDENT_COMMAND_HANDLER_QUEUE = 'aws:arn:StudentCommandHandlerQueue'
    process.env.CLUB_MEMBERSHIP_COMMAND_HANDLER_QUEUE = 'aws:arn:ClubMembershipCommandHandlerQueue'
    process.env.REST_API_BASE_URL = 'http://localhost'

    const {Configuration} = require('../src/configuration')

    it('should return the configuration ProfessorCommandHandlerQueue', () => {
        expect(Configuration.ProfessorCommandHandlerQueue).toEqual('aws:arn:ProfessorCommandHandlerQueue')
    })

    it('should return the configuration ClubCommandHandlerQueue', () => {
        expect(Configuration.ClubCommandHandlerQueue).toEqual('aws:arn:ClubCommandHandlerQueue')
    })

    it('should return the configuration StudentCommandHandlerQueue', () => {
        expect(Configuration.StudentCommandHandlerQueue).toEqual('aws:arn:StudentCommandHandlerQueue')
    })

    it('should return the configuration ClubMembershipCommandHandlerQueue', () => {
        expect(Configuration.ClubMembershipCommandHandlerQueue).toEqual('aws:arn:ClubMembershipCommandHandlerQueue')
    })

    it('should return the configuration RestAPIBaseURL', () => {
        expect(Configuration.RestAPIBaseURL).toEqual('http://localhost')
    })
})
