#!/usr/bin/env node
const cdk = require('@aws-cdk/core')
const {WebsocketStack} = require('../lib/websocket-stack')
const {RestStack} = require('../lib/rest-stack')

const app = new cdk.App();
const env = app.node.tryGetContext('env')

new WebsocketStack(env, app, `Gateway-Websocket-Stack-${env}`)
new RestStack(env, app, `Gateway-Rest-Stack-${env}`)