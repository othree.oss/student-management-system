const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const apigateway = require('@aws-cdk/aws-apigateway')
const iam = require('@aws-cdk/aws-iam')

class RestStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const getAllProfessorsARN = cdk.Fn.importValue(`Professor-RestFn-GetAllProfessorsHandler-${env}`)
        const getAllProfessorsHandler = lambda.Function.fromFunctionArn(this, `Professor-RestFn-GetAllProfessorsHandler-${env}`, getAllProfessorsARN)

        const getProfessorARN = cdk.Fn.importValue(`Professor-RestFn-GetProfessorHandler-${env}`)
        const getProfessorHandler = lambda.Function.fromFunctionArn(this, `Professor-RestFn-GetProfessorHandler-${env}`, getProfessorARN)

        const getAllClubsARN = cdk.Fn.importValue(`Club-RestFn-GetAllClubsHandler-${env}`)
        const getAllClubsHandler = lambda.Function.fromFunctionArn(this, `Club-RestFn-GetAllClubsHandler-${env}`, getAllClubsARN)

        const getClubARN = cdk.Fn.importValue(`Club-RestFn-GetClubHandler-${env}`)
        const getClubHandler = lambda.Function.fromFunctionArn(this, `Club-RestFn-GetClubHandler-${env}`, getClubARN)

        const getAllStudentsARN = cdk.Fn.importValue(`Student-RestFn-GetAllStudentsHandler-${env}`)
        const getAllStudentsHandler = lambda.Function.fromFunctionArn(this, `Student-RestFn-GetStudentsHandler-${env}`, getAllStudentsARN)

        const getStudentARN = cdk.Fn.importValue(`Student-RestFn-GetStudentHandler-${env}`)
        const getStudentHandler = lambda.Function.fromFunctionArn(this, `Student-RestFn-GetStudentHandler-${env}`, getStudentARN)

        const getMembershipARN = cdk.Fn.importValue(`Club-Membership-RestFn-GetByClubAndStudentHandler-${env}`)
        const getMembershipHandler = lambda.Function.fromFunctionArn(this, `Club-Membership-RestFn-GetByClubAndStudentHandler-${env}`, getMembershipARN)

        const api = new apigateway.RestApi(this, `Gateway-Rest-API-${env}`, {
            restApiName: `Gateway-Rest-API-${env}`,
            deployOptions: {
                stageName: env
            },
            defaultCorsPreflightOptions: {
                allowOrigins: apigateway.Cors.ALL_ORIGINS,
                allowMethods: ['GET'],
                allowHeaders: apigateway.Cors.DEFAULT_HEADERS
            }
        })

        const v1Resource = api.root.addResource('v1')
        const professorsResource = v1Resource.addResource('professors')
        const professorIdResource = professorsResource.addResource('{id}')

        const clubsResource = v1Resource.addResource('clubs')
        const clubIdResource = clubsResource.addResource('{id}')

        const studentsResource = v1Resource.addResource('students')
        const studentIdResource = studentsResource.addResource('{id}')

        const clubStudentsResource = clubIdResource.addResource('students')
        const clubStudentIdResource = clubStudentsResource.addResource('{studentId}')

        const professorsIntegration = new apigateway.LambdaIntegration(getAllProfessorsHandler)
        const professorIdIntegration = new apigateway.LambdaIntegration(getProfessorHandler)

        const clubsIntegration = new apigateway.LambdaIntegration(getAllClubsHandler)
        const clubIdIntegration = new apigateway.LambdaIntegration(getClubHandler)

        const studentsIntegration = new apigateway.LambdaIntegration(getAllStudentsHandler)
        const studentIdIntegration = new apigateway.LambdaIntegration(getStudentHandler)

        const clubStudentIdIntegration = new apigateway.LambdaIntegration(getMembershipHandler)

        professorsResource.addMethod('GET', professorsIntegration)
        professorIdResource.addMethod('GET', professorIdIntegration)

        clubsResource.addMethod('GET', clubsIntegration)
        clubIdResource.addMethod('GET', clubIdIntegration)

        studentsResource.addMethod('GET', studentsIntegration)
        studentIdResource.addMethod('GET', studentIdIntegration)

        clubStudentIdResource.addMethod('GET', clubStudentIdIntegration)

        new cdk.CfnOutput(this, `Export-Gateway-Rest-API-${env}`, {
            exportName: `Gateway-Rest-API-${env}`,
            value: `https://${api.restApiId}.execute-api.${this.region}.amazonaws.com/${env}`
        })
    }
}

module.exports = {RestStack}
