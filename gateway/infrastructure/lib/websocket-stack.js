const cdk = require('@aws-cdk/core')
const sqs = require('@aws-cdk/aws-sqs')
const gateway = require('@aws-cdk/aws-apigatewayv2')
const {CfnApi, CfnDeployment, CfnIntegration, CfnRoute, CfnStage} = require('@aws-cdk/aws-apigatewayv2')
const lambda = require('@aws-cdk/aws-lambda')
const {LambdaWebSocketIntegration} = require('@aws-cdk/aws-apigatewayv2-integrations')
const iam = require('@aws-cdk/aws-iam')

class WebsocketStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const restApiBaseUrl = cdk.Fn.importValue(`Gateway-Rest-API-${env}`)

        const professorCommandsSqsArn = cdk.Fn.importValue(`Professor-SQS-Commands-${env}`)
        const clubCommandsSqsArn = cdk.Fn.importValue(`Club-SQS-Commands-${env}`)
        const studentCommandsSqsArn = cdk.Fn.importValue(`Student-SQS-Commands-${env}`)
        const clubMembershipCommandsSqsArn = cdk.Fn.importValue(`Club-Membership-SQS-Commands-${env}`)

        const professorCommandsSqs = sqs.Queue.fromQueueArn(this, `Professor-SQS-Commands-${env}`, professorCommandsSqsArn)
        const clubCommandsSqs = sqs.Queue.fromQueueArn(this, `Club-SQS-Commands-${env}`, clubCommandsSqsArn)
        const studentCommandsSqs = sqs.Queue.fromQueueArn(this, `Student-SQS-Commands-${env}`, studentCommandsSqsArn)
        const clubMembershipCommandSqs = sqs.Queue.fromQueueArn(this, `Club-Membership-SQS-Commands-${env}`, clubMembershipCommandsSqsArn)

        const api = new gateway.CfnApi(this, `Gateway-WS-API-${env}`, {
            name: `Gateway-WS-API-${env}`,
            protocolType: 'WEBSOCKET',
            routeSelectionExpression: '$request.body.action',
        })

        const requestProfessorCreationHandler = new lambda.Function(this, `Gateway-WsFn-RequestProfessorCreationHandler-${env}`, {
            functionName: `Gateway-WsFn-RequestProfessorCreationHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.professor.requestProfessorCreationHandler',
            environment: {
                PROFESSOR_COMMAND_HANDLER_QUEUE: professorCommandsSqs.queueUrl
            }
        })
        professorCommandsSqs.grantSendMessages(requestProfessorCreationHandler)

        const requestPostRatingHandler = new lambda.Function(this, `Gateway-WsFn-RequestPostRatingHandler-${env}`, {
            functionName: `Gateway-WsFn-RequestPostRatingHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.professor.requestPostRatingHandler',
            environment: {
                PROFESSOR_COMMAND_HANDLER_QUEUE: professorCommandsSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        professorCommandsSqs.grantSendMessages(requestPostRatingHandler)

        const requestClubCreationHandler = new lambda.Function(this, `Gateway-WsFn-RequestClubCreationHandler-${env}`, {
            functionName: `Gateway-WsFn-RequestClubCreationHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.club.requestClubCreationHandler',
            environment: {
                CLUB_COMMAND_HANDLER_QUEUE: clubCommandsSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        clubCommandsSqs.grantSendMessages(requestClubCreationHandler)

        const requestClubOpeningHandler = new lambda.Function(this, `Gateway-WsFn-RequestClubOpeningHandler-${env}`, {
            functionName: `Gateway-WsFn-RequestClubOpeningHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.club.requestClubOpeningHandler',
            environment: {
                CLUB_COMMAND_HANDLER_QUEUE: clubCommandsSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        clubCommandsSqs.grantSendMessages(requestClubOpeningHandler)

        const requestClubClosingHandler = new lambda.Function(this, `Gateway-WsFn-RequestClubClosingHandler-${env}`, {
            functionName: `Gateway-WsFn-RequestClubClosingHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.club.requestClubClosingHandler',
            environment: {
                CLUB_COMMAND_HANDLER_QUEUE: clubCommandsSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        clubCommandsSqs.grantSendMessages(requestClubClosingHandler)

        const requestStudentCreationHandler = new lambda.Function(this, `Gateway-WsFn-RequestStudentCreation-${env}`, {
            functionName: `Gateway-WsFn-RequestStudentCreation-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.student.requestStudentCreationHandler',
            environment: {
                STUDENT_COMMAND_HANDLER_QUEUE: studentCommandsSqs.queueUrl
            }
        })
        studentCommandsSqs.grantSendMessages(requestStudentCreationHandler)

        const requestStudentEnrollmentHandler = new lambda.Function(this, `Gateway-WsFn-RequestStudentEnrollment-${env}`, {
            functionName: `Gateway-WsFn-RequestStudentEnrollment-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.clubMembership.requestStudentEnrollmentHandler',
            environment: {
                CLUB_MEMBERSHIP_COMMAND_HANDLER_QUEUE: clubMembershipCommandSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        clubMembershipCommandSqs.grantSendMessages(requestStudentEnrollmentHandler)

        const requestGradedAssignmentCreationHandler = new lambda.Function(this, `Gateway-WsFn-RequestGradedAssignmentCreation-${env}`, {
            functionName: `Gateway-WsFn-RequestGradedAssignmentCreation-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.clubMembership.requestGradedAssignmentCreationHandler',
            environment: {
                CLUB_MEMBERSHIP_COMMAND_HANDLER_QUEUE: clubMembershipCommandSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        clubMembershipCommandSqs.grantSendMessages(requestGradedAssignmentCreationHandler)

        const requestGradedAssignmentRemovalHandler = new lambda.Function(this, `Gateway-WsFn-RequestGradedAssignmentRemoval-${env}`, {
            functionName: `Gateway-WsFn-RequestGradedAssignmentRemoval-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/entrypoint.ws.clubMembership.requestGradedAssignmentRemovalHandler',
            environment: {
                CLUB_MEMBERSHIP_COMMAND_HANDLER_QUEUE: clubMembershipCommandSqs.queueUrl,
                REST_API_BASE_URL: restApiBaseUrl
            }
        })
        clubMembershipCommandSqs.grantSendMessages(requestGradedAssignmentRemovalHandler)

        const wsLambdas = [
            requestProfessorCreationHandler,
            requestPostRatingHandler,
            requestClubCreationHandler,
            requestClubOpeningHandler,
            requestClubClosingHandler,
            requestStudentCreationHandler,
            requestStudentEnrollmentHandler,
            requestGradedAssignmentCreationHandler,
            requestGradedAssignmentRemovalHandler
        ]

        wsLambdas.forEach(lambda => {
            lambda.addToRolePolicy(new iam.PolicyStatement({
                effect: iam.Effect.ALLOW,
                actions: ['execute-api:Invoke', 'execute-api:ManageConnections'],
                resources: ['arn:aws:execute-api:*:*:*']
            }))
        })

        const policy = new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            resources: wsLambdas.map(_ => _.functionArn),
            actions: ['lambda:InvokeFunction']
        })

        const role = new iam.Role(this, `Gateway-IAM-Role-${env}`, {
            roleName: `Gateway-IAM-Role-${env}`,
            assumedBy: new iam.ServicePrincipal('apigateway.amazonaws.com')
        })
        role.addToPolicy(policy)

        const createRoute = (api, name, routeKey, lambda, roleArn) => {
            const integration = new CfnIntegration(this, `Gateway-WS-${name}Integration-${env}`, {
                apiId: api.ref,
                integrationType: 'AWS_PROXY',
                integrationUri: 'arn:aws:apigateway:' + this.region + ':lambda:path/2015-03-31/functions/' + lambda.functionArn + '/invocations',
                credentialsArn: roleArn
            })

            const route = new CfnRoute(this, `Gateway-WS-${name}Route-${env}`, {
                apiId: api.ref,
                routeKey: routeKey,
                authorizationType: 'NONE',
                target: "integrations/" + integration.ref,
            })

            return route
        }

        const createProfessorRoute = createRoute(api, 'RequestProfessorCreation', 'createprofessor', requestProfessorCreationHandler, role.roleArn)
        const postRatingRoute = createRoute(api, 'RequestPostRating', 'postrating', requestPostRatingHandler, role.roleArn)
        const createClubRoute = createRoute(api, 'RequestClubCreation', 'createclub', requestClubCreationHandler, role.roleArn)
        const openClubRoute = createRoute(api, 'RequestClubOpening', 'openclub', requestClubOpeningHandler, role.roleArn)
        const closeClubRoute = createRoute(api, 'RequestClubClosing', 'closeclub', requestClubClosingHandler, role.roleArn)
        const createStudentRoute = createRoute(api, 'RequestStudentCreation', 'createstudent', requestStudentCreationHandler, role.roleArn)
        const enrollStudentRoute = createRoute(api, 'RequestStudentEnrollment', 'enrollstudent', requestStudentEnrollmentHandler, role.roleArn)
        const addGradedAssignmentRoute = createRoute(api, 'RequestGradedAssignmentCreation', 'addgradedassignment', requestGradedAssignmentCreationHandler, role.roleArn)
        const removeGradedAssignmentRoute = createRoute(api, 'RequestGradedAssignmentRemoval', 'removegradedassignment', requestGradedAssignmentRemovalHandler, role.roleArn)

        const deployment = new CfnDeployment(this, `Gateway-WS-Deployment-${env}`, {
            apiId: api.ref
        })

        new CfnStage(this, `Gateway-WS-Stage-${env}`, {
            apiId: api.ref,
            autoDeploy: true,
            deploymentId: deployment.ref,
            stageName: env
        })

        const dependencies = new cdk.ConcreteDependable()
        dependencies.add(createProfessorRoute)
        dependencies.add(postRatingRoute)
        dependencies.add(createClubRoute)
        dependencies.add(openClubRoute)
        dependencies.add(closeClubRoute)
        dependencies.add(createStudentRoute)
        dependencies.add(enrollStudentRoute)
        dependencies.add(addGradedAssignmentRoute)
        dependencies.add(removeGradedAssignmentRoute)
        deployment.node.addDependency(dependencies)
    }
}

module.exports = {WebsocketStack}
