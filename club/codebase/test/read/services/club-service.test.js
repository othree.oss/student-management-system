const {ClubService} = require('../../../src/read/services/club-service')
const {EVENT_TYPES} = require('../../../src/shared/event-types')
const {CLUB_STATUS} = require('../../../src/shared/club-status')
const {Optional} = require('@othree.io/optional')

describe('ClubService', () => {
    describe('updateProjection', () => {

        const updateEvents = [EVENT_TYPES.CLUB_CREATED, EVENT_TYPES.CLUB_OPENED, EVENT_TYPES.CLUB_CLOSED]

        updateEvents.forEach(eventType => {
            it(`should update the projection when the event is ${eventType}`, async () => {
                const state = {
                    id: '1337',
                    name: 'Chess',
                    professorId: '007',
                    status: CLUB_STATUS.OPENED
                }

                const repository = {
                    upsert: async (club) => {
                        expect(club).toStrictEqual(state)

                        return Optional({...club})
                    }
                }

                const service = ClubService(repository)

                const maybeClub = await service.updateProjection({type: eventType}, state)

                expect(maybeClub.isPresent).toBeTruthy()
                expect(maybeClub.get()).toStrictEqual(state)
            })
        })

        it('should return an empty optional if the event is unhandled', async () => {
            const state = {
                id: 'abc'
            }

            const service = ClubService({})

            const event = {
                type: 'UNEXPECTED'
            }

            const maybeClub = await service.updateProjection(event, state)

            expect(maybeClub.isPresent).toBeFalsy()
        })
    })
})