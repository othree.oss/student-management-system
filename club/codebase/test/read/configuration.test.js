describe('Configuration', () => {
    process.env.CLUB_PROJECTION_TABLE = 'aws:arn:ClubProjectionTable'

    const {Configuration} = require('../../src/read/configuration')

    it('should return the configured ProjectionTable', () => {
        expect(Configuration.ProjectionTable).toEqual('aws:arn:ClubProjectionTable')
    })
})
