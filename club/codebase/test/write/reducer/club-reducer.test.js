const {InitialState, reducer} = require('../../../src/write/reducer/club-reducer')
const {CLUB_STATUS} = require('../../../src/shared/club-status')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('InitialState', () => {
    it('should be initialized', () => {
        expect(InitialState).toStrictEqual({
            status: CLUB_STATUS.CLOSED
        })
    })
})

describe('reducer', () => {
    it('should handle a CLUB_CREATED event', () => {
        const event = {
            contextId: '1337',
            type: EVENT_TYPES.CLUB_CREATED,
            body: {
                name: 'Chess',
                professorId: '007'
            }
        }

        const state = reducer(InitialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            name: 'Chess',
            professorId: '007',
            status: CLUB_STATUS.CLOSED
        })
    })

    it('should handle a CLUB_OPENED event', () => {
        const initialState = {
            id: '1337',
            name: 'Chess',
            professorId: '007',
            status: CLUB_STATUS.CLOSED
        }
        const event = {
            contextId: '1337',
            type: EVENT_TYPES.CLUB_OPENED,
            body: {}
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            name: 'Chess',
            professorId: '007',
            status: CLUB_STATUS.OPENED
        })
    })

    it('should handle a CLUB_CLOSED event', () => {
        const initialState = {
            id: '1337',
            name: 'Chess',
            professorId: '007',
            status: CLUB_STATUS.OPENED
        }
        const event = {
            contextId: '1337',
            type: EVENT_TYPES.CLUB_CLOSED,
            body: {}
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            name: 'Chess',
            professorId: '007',
            status: CLUB_STATUS.CLOSED
        })
    })
})