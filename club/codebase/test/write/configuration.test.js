describe('Configuration', () => {
    process.env.CLUB_EVENTS_TABLE = 'aws:arn:ClubEventsTable'
    process.env.CLUB_EVENTS_TOPIC = 'aws:arn:ClubEventsTopic'

    const {Configuration} = require('../../src/write/configuration')

    it('should return the configured EventsTopic', () => {
        expect(Configuration.EventsTopic).toEqual('aws:arn:ClubEventsTopic')
    })

    it('should return the configured ProfessorEventsTable', () => {
        expect(Configuration.ClubEventsTable).toEqual('aws:arn:ClubEventsTable')
    })
})
