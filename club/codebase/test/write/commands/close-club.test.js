const {ClubCommandHandler} = require('../../../src/write/commands/club-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('CloseClub', () => {

    it('should return a CLUB_CLOSED event', async () => {
        const handler = ClubCommandHandler({})

        const command = {
            contextId: '1337',
            type: COMMAND_TYPES.CLOSE_CLUB,
            body: {}
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.CLUB_CLOSED,
            body: {}
        })
    })
})
