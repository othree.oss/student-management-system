const {ClubCommandHandler} = require('../../../src/write/commands/club-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('OpenClub', () => {

    it('should return a CLUB_OPENED event', async () => {
        const handler = ClubCommandHandler({})

        const command = {
            contextId: '1337',
            type: COMMAND_TYPES.OPEN_CLUB,
            body: {}
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.CLUB_OPENED,
            body: {}
        })
    })
})
