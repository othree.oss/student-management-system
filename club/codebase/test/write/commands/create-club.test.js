const {ClubCommandHandler} = require('../../../src/write/commands/club-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('CreateClub', () => {
    const uuid = () => '1337'

    it('should return a CLUB_CREATED event', async () => {
        const handler = ClubCommandHandler(uuid)

        const command = {
            type: COMMAND_TYPES.CREATE_CLUB,
            body: {
                name: 'Chess',
                professorId: '007'
            }
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.CLUB_CREATED,
            body: {
                name: 'Chess',
                professorId: '007'
            }
        })
    })
})
