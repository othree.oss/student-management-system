const AWS = require('aws-sdk')
const {Configuration} = require('./configuration')
const {ClubService} = require('./services/club-service')
const {SqsUpdateProjectionHandler, BaseProjectionRestHandlers} = require('utils').lambda
const {BaseProjectionRepository} = require('utils').dynamodb
const {BaseProjectionService} = require('utils').services

const documentClient = new AWS.DynamoDB.DocumentClient()

const clubRepository = BaseProjectionRepository(documentClient, Configuration)
const clubService = {
    ...ClubService(clubRepository),
    ...BaseProjectionService(clubRepository)
}
const clubUpdateProjectionHandler = SqsUpdateProjectionHandler(clubService)
const clubRestHandlers = BaseProjectionRestHandlers(clubService)

module.exports = {
    rest: {
        ...clubRestHandlers
    },
    sqs: {
        ...clubUpdateProjectionHandler
    }
}