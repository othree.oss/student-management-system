const {EVENT_TYPES} = require('../../shared/event-types')
const {Optional} = require('@othree.io/optional')

function ClubService(clubRepository) {
    return Object.freeze({
        updateProjection: async (event, state) => {
            switch(event.type) {
                case EVENT_TYPES.CLUB_CLOSED:
                case EVENT_TYPES.CLUB_OPENED:
                case EVENT_TYPES.CLUB_CREATED:
                    return clubRepository.upsert(state)
                default:
                    return Optional()
            }
        }
    })
}

module.exports = {ClubService}
