const Configuration = Object.freeze({
    ProjectionTable: process.env.CLUB_PROJECTION_TABLE
})

module.exports = {Configuration}
