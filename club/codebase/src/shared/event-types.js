const EVENT_TYPES = Object.freeze({
    CLUB_CREATED: 'ClubCreated',
    CLUB_OPENED: 'ClubOpened',
    CLUB_CLOSED: 'ClubClosed'
})

module.exports = {EVENT_TYPES}
