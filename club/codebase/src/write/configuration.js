const Configuration = Object.freeze({
    ClubEventsTable: process.env.CLUB_EVENTS_TABLE,
    EventsTopic: process.env.CLUB_EVENTS_TOPIC
})

module.exports = {Configuration}
