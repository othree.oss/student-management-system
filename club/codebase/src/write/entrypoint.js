const {Aggregate, EventSourcing, DynamoEventRepository, SnsEventNotifier, DateProvider } = require('@othree.io/chisel')
const AWS = require('aws-sdk')
const {v4: uuid} = require('uuid')
const {reducer, InitialState} = require('./reducer/club-reducer')
const {ClubCommandHandler} = require('./commands/club-command-handler')
const {Configuration} = require('./configuration')
const {ResponseHandler} = require('utils').websockets
const {SqsCommandHandler} = require('utils').lambda

const documentClient = new AWS.DynamoDB.DocumentClient()
const eventRepository = DynamoEventRepository(
    documentClient,
    Configuration.ClubEventsTable
)
const snsClient = new AWS.SNS()
const eventNotifier = SnsEventNotifier(Configuration.EventsTopic, 'club', snsClient, true)

const eventSourcing = EventSourcing(
    eventRepository,
    reducer,
    eventNotifier,
    uuid,
    DateProvider,
    {...InitialState}
)
const apiGatewayManagementApiBuilder = (endpoint) => {
    return new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: endpoint
    })
}
const responseHandler = ResponseHandler(apiGatewayManagementApiBuilder)

const commandHandler = ClubCommandHandler(uuid)

const clubAggregate = Aggregate(
    eventSourcing,
    commandHandler.handle
)

const clubCommandHandler = SqsCommandHandler(clubAggregate, responseHandler)

module.exports = {
    sqs: {
        ...clubCommandHandler
    }
}