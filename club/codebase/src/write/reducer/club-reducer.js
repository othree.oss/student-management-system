const {CLUB_STATUS} = require('../../shared/club-status')
const {EVENT_TYPES} = require('../../shared/event-types')

const InitialState = {
    status: CLUB_STATUS.CLOSED
}

const reducer = (state, event) => {
    switch (event.type) {
        case EVENT_TYPES.CLUB_CREATED:
            return clubCreated(state, event)
        case EVENT_TYPES.CLUB_CLOSED:
            return clubClosed(state, event)
        case EVENT_TYPES.CLUB_OPENED:
            return clubOpened(state, event)
        default:
            return state
    }
}

const clubCreated = (state, event) => {
    const {body: club} = event
    return {
        ...state,
        id: event.contextId,
        name: club.name,
        professorId: club.professorId
    }
}

const clubClosed = (state, event) => {
    return {
        ...state,
        status: CLUB_STATUS.CLOSED
    }
}

const clubOpened = (state, event) => {
    return {
        ...state,
        status: CLUB_STATUS.OPENED
    }
}

module.exports = {InitialState, reducer}
