const {EVENT_TYPES} = require('../../shared/event-types')
const {createEvent} = require('@othree.io/chisel')

function CreateClub(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Creating club', command)

            const {body: club} = command

            const clubId = uuid()

            return createEvent(
                clubId,
                EVENT_TYPES.CLUB_CREATED,
                {
                    name: club.name,
                    professorId: club.professorId
                }
            )
        }
    })
}

module.exports = {CreateClub}
