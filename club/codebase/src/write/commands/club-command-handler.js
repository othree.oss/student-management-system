const {COMMAND_TYPES} = require('./command-types')
const {CreateClub} = require('./create-club')
const {OpenClub} = require('./open-club')
const {CloseClub} = require('./close-club')

function ClubCommandHandler(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Handling command', command, 'Current state', state)
            switch (command.type) {
                case COMMAND_TYPES.CREATE_CLUB:
                    const createClub = CreateClub(uuid)
                    return createClub.handle(state, command)
                case COMMAND_TYPES.OPEN_CLUB:
                    const openClub = OpenClub()
                    return openClub.handle(state, command)
                case COMMAND_TYPES.CLOSE_CLUB:
                    const closeClub = CloseClub()
                    return closeClub.handle(state, command)
            }
        }
    })
}

module.exports = {ClubCommandHandler}
