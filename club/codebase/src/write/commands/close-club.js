const {EVENT_TYPES} = require('../../shared/event-types')
const {createEvent} = require('@othree.io/chisel')

function CloseClub() {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Closing club', command)

            return createEvent(
                command.contextId,
                EVENT_TYPES.CLUB_CLOSED,
                {}
            )
        }
    })
}

module.exports = {CloseClub}
