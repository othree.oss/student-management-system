const {EVENT_TYPES} = require('../../shared/event-types')
const {createEvent} = require('@othree.io/chisel')

function OpenClub() {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Opening club', command)

            return createEvent(
                command.contextId,
                EVENT_TYPES.CLUB_OPENED,
                {}
            )
        }
    })
}

module.exports = {OpenClub}
