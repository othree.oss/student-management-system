#!/usr/bin/env node
const cdk = require('@aws-cdk/core')
const {ClubCoreStack} = require('../lib/club-core-stack')
const {ClubWriteStack} = require('../lib/club-write-stack')
const {ClubReadStack} = require('../lib/club-read-stack')

const app = new cdk.App();
const env = app.node.tryGetContext('env')

new ClubCoreStack(env, app, `Club-Core-Stack-${env}`)
new ClubWriteStack(env, app, `Club-Write-Stack-${env}`)
new ClubReadStack(env, app, `Club-Read-Stack-${env}`)
