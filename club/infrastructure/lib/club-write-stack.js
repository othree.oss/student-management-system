const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const iam = require('@aws-cdk/aws-iam')

class ClubWriteStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const clubTopicARN = cdk.Fn.importValue(`Club-Topic-${env}`)
        const clubTopic = sns.Topic.fromTopicArn(this, `Club-Topic-${env}`, clubTopicARN)

        const clubEventsTable = new dynamodb.Table(this, `Club-Events-${env}`, {
            tableName: `Club-Events-${env}`,
            partitionKey: {name: 'contextId', type: dynamodb.AttributeType.STRING},
            sortKey: {name: 'eventDate', type: dynamodb.AttributeType.NUMBER},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST
        })

        const clubCommandsDLQ = new sqs.Queue(this, `Club-SQS-CommandsDLQ-${env}`, {
            queueName: `Club-SQS-CommandsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const clubCommandsQ = new sqs.Queue(this, `Club-SQS-Commands-${env}`, {
            queueName: `Club-SQS-Commands-${env}.fifo`,
            deadLetterQueue: {
                queue: clubCommandsDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        const commandHandler = new lambda.Function(this, `Club-SqsFn-CommandHandler-${env}`, {
            functionName: `Club-SqsFn-CommandHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/write/entrypoint.sqs.commandHandler',
            environment: {
                CLUB_EVENTS_TABLE: clubEventsTable.tableName,
                CLUB_EVENTS_TOPIC: clubTopicARN
            }
        })
        clubEventsTable.grantReadWriteData(commandHandler)
        clubTopic.grantPublish(commandHandler)

        commandHandler.addEventSource(
            new SqsEventSource(
                clubCommandsQ
            )
        )

        commandHandler.addToRolePolicy(new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: ['execute-api:Invoke', 'execute-api:ManageConnections'],
            resources: ['arn:aws:execute-api:*:*:*']
        }))

        new cdk.CfnOutput(this, `Export-Club-SQS-Commands-${env}`, {
            exportName: `Club-SQS-Commands-${env}`,
            value: clubCommandsQ.queueArn
        })

        new cdk.CfnOutput(this, `Export-Club-SqsFn-CommandHandler-${env}`, {
            exportName: `Club-SqsFn-CommandHandler-${env}`,
            value: commandHandler.functionArn
        })
    }
}

module.exports = {ClubWriteStack}
