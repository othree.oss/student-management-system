const cdk = require('@aws-cdk/core');
const sns = require('@aws-cdk/aws-sns');

class ClubCoreStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props);

        const topic = new sns.Topic(this, `Club-Topic-${env}`, {
            topicName: `Club-Topic-${env}`,
            displayName: 'Club Topic',
            fifo: true,
            contentBasedDeduplication: true
        })

        new cdk.CfnOutput(this, `Output-Club-Topic-${env}`, {
            value: topic.topicArn,
            exportName: `Club-Topic-${env}`
        })
    }
}

module.exports = {ClubCoreStack}
