const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const subs = require('@aws-cdk/aws-sns-subscriptions')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const iam = require('@aws-cdk/aws-iam')

class ClubReadStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const clubTopicARN = cdk.Fn.importValue(`Club-Topic-${env}`)
        const clubTopic = sns.Topic.fromTopicArn(this, `Club-Topic-${env}`, clubTopicARN)

        const clubDLQ = new sqs.Queue(this, `Club-SQS-EventsDLQ-${env}`, {
            queueName: `Club-SQS-EventsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const clubEventsQ = new sqs.Queue(this, `Club-SQS-Events-${env}`, {
            queueName: `Club-SQS-Events-${env}.fifo`,
            deadLetterQueue: {
                queue: clubDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        clubTopic.addSubscription(
            new subs.SqsSubscription(
                clubEventsQ,
                {
                    filterPolicy: {
                        bc: sns.SubscriptionFilter.stringFilter({
                            whitelist: ['club']
                        })
                    }
                }
            )
        )

        const clubProjectionTable = new dynamodb.Table(this, `Club-Projection-${env}`, {
            tableName: `Club-Projection-${env}`,
            partitionKey: {name: 'id', type: dynamodb.AttributeType.STRING},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        })

        const updateProjectionHandler = new lambda.Function(this, `Club-SqsFn-UpdateProjectionHandler-${env}`, {
            functionName: `Club-SqsFn-UpdateProjectionHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.sqs.updateProjectionHandler',
            environment: {
                CLUB_PROJECTION_TABLE: clubProjectionTable.tableName
            }
        })
        clubProjectionTable.grantReadWriteData(updateProjectionHandler)

        updateProjectionHandler.addEventSource(
            new SqsEventSource(
                clubEventsQ
            )
        )

        const getClubHandler = new lambda.Function(this, `Club-RestFn-GetClubHandler-${env}`, {
            functionName: `Club-RestFn-GetClubHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getHandler',
            environment: {
                CLUB_PROJECTION_TABLE: clubProjectionTable.tableName
            }
        })
        clubProjectionTable.grantReadData(getClubHandler)
        getClubHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        const getAllClubsHandler = new lambda.Function(this, `Club-RestFn-GetAllClubsHandler-${env}`, {
            functionName: `Club-RestFn-GetAllClubsHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getAllHandler',
            environment: {
                CLUB_PROJECTION_TABLE: clubProjectionTable.tableName
            }
        })
        clubProjectionTable.grantReadData(getAllClubsHandler)
        getAllClubsHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        new cdk.CfnOutput(this, `Output-Club-RestFn-GetClubHandler-${env}`, {
            exportName: `Club-RestFn-GetClubHandler-${env}`,
            value: getClubHandler.functionArn
        })

        new cdk.CfnOutput(this, `Output-Club-RestFn-GetAllClubsHandler-${env}`, {
            exportName: `Club-RestFn-GetAllClubsHandler-${env}`,
            value: getAllClubsHandler.functionArn
        })
    }
}

module.exports = {ClubReadStack}
