# Chisel reference application

This application is a reference application for [Chisel](https://gitlab.com/othree.oss/chisel).

## Application structure

The sample application consists of four microservices:
* Professor
* Club
* Student
* Club-Membership

Also a gateway service using Websockets and REST apis.

## Infrastructure

This application is meant to be deployed in AWS using the cdk project inside each of the microservices and the gateway.

To deploy each of the services do the following:

```bash
cd {microservice}
cd codebase
npm install
cd ../infrastructure
npm install
npm run cdk -c env=Dev deploy '*'
```

This will get the microservice deployed to the AWS you previously configured with AWS CLI 

If you want to deploy all projects there's a handy `deploy-all.sh` that you can execute from the repository's root.
