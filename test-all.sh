#!/bin/bash
BASE_DIR=`pwd`

test() {
  cd $1/codebase
  npm install
  npm run test
  cd $BASE_DIR
}

echo '---------- TESTING UTILS ----------'
cd utils
npm install
npm run test
cd $BASE_DIR

echo '---------- TESTING PROFESSOR MICROSERVICE ----------'
test 'professor'

echo '---------- TESTING STUDENT MICROSERVICE ----------'
test 'student'

echo '---------- TESTING CLUB MICROSERVICE ----------'
test 'club'

echo '---------- TESTING CLUB MEMBERSHIP MICROSERVICE ----------'
test 'club-membership'

echo '---------- TESTING GATEWAY ----------'
test 'gateway'