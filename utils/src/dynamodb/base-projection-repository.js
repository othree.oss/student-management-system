const {Try} = require('@othree.io/optional')

function BaseProjectionRepository(documentClient, configuration) {
    return Object.freeze({
        upsert: async (entity) => {
            console.log('Upserting entity', entity, 'to table', configuration.ProjectionTable)
            return Try(async () => {
                const putResult = await documentClient.put({
                    TableName: configuration.ProjectionTable,
                    Item: entity,
                    ReturnValues: 'NONE',
                    ReturnConsumedCapacity: 'NONE',
                    ReturnItemCollectionMetrics: 'NONE'
                }).promise()

                console.log('Entity upsert result', entity, putResult)

                return entity
            })
        },
        get: async (entityId) => {
            console.log('Getting entity', entityId, 'from table', configuration.ProjectionTable)
            return Try(async () => {
                const result = await documentClient.get({
                    TableName: configuration.ProjectionTable,
                    Key: {
                        id: entityId
                    }
                }).promise()

                console.log('Get professor result', entityId, result)

                return result.Item
            })
        },
        getAll: async () => {
            console.log('Getting all entities from table', configuration.ProjectionTable)
            return Try(async () => {
                const result = await documentClient.scan({
                    TableName: configuration.ProjectionTable
                }).promise()

                console.log('Get all entities result', result)

                return result.Items
            })
        }
    })
}

module.exports = {BaseProjectionRepository}
