function SqsUpdateProjectionHandler(projectionService) {
    return Object.freeze({
        updateProjectionHandler: async (event) => {
            console.log('Processing sqs event', event)

            const promises = event.Records.map(async record => {
                console.log('Processing record', record)

                const {body} = record

                const sqsEvent = JSON.parse(body)

                const eventMessage = JSON.parse(sqsEvent.Message)

                const maybeEntity = await projectionService.updateProjection(eventMessage.event, eventMessage.state)

                if (maybeEntity.isEmpty) {
                    console.log('Failed to process event', eventMessage.event.eventId, maybeEntity.getError())
                }
                return maybeEntity.orElse(undefined)
            })

            return Promise.all(promises)
        }
    })
}

module.exports = {SqsUpdateProjectionHandler}
