function SqsCommandHandler(aggregate, responseHandler) {
    return Object.freeze({
        commandHandler: async (event) => {
            console.log('Processing sqs event', event)

            const promises = event.Records.map(async record => {
                console.log('Processing record', record)

                const {body} = record

                const command = JSON.parse(body)

                const es = await aggregate.handle(command)

                await Promise.all(es.newEvents.map(async event => {
                    const maybeResponse = await responseHandler.respond(es.state, event)
                    if (maybeResponse.isEmpty) {
                        console.log('Failed to send response', maybeResponse.getError())
                    }

                    return maybeResponse
                }))

                return true
            })

            return Promise.all(promises)
        }
    })
}

module.exports = {SqsCommandHandler}
