const {createResponse} = require('../gateway/rest-utils')

function BaseProjectionRestHandlers(baseProjectionService) {
    return Object.freeze({
        getHandler: async (event) => {
            console.log('Getting entity', event)
            const {id} = event.pathParameters

            const maybeEntity = await baseProjectionService.get(id)

            if (maybeEntity.isEmpty) {
                console.log('Failed to get entity', id, maybeEntity.getError())
            }

            return maybeEntity.map(entity => createResponse(200, entity))
                .orElse(createResponse(404))
        },
        getAllHandler: async (event) => {
            console.log('Getting all entities', event)

            const maybeEntities = await baseProjectionService.getAll()

            if (maybeEntities.isEmpty) {
                console.log('Failed to get all entities', maybeEntities.getError())
            }

            return maybeEntities.map(entities => createResponse(200, entities))
                .orElse(createResponse(500))
        }
    })
}

module.exports = {BaseProjectionRestHandlers}
