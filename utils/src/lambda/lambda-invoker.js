const {Try} = require('@othree.io/optional')

class InvocationError extends Error {
    constructor(errorType, ...params) {
        super(...params)

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, InvocationError)
        }

        this.errorType = errorType
    }
}

function LambdaInvoker(lambda) {
    return {
        invoke: (fn, payload) => {
            const params = {
                FunctionName: fn,
                Payload: JSON.stringify(payload)
            }

            return Try(async () => {
                const result = await lambda.invoke(params).promise()

                if (result.StatusCode === 200) {
                    const payload = JSON.parse(result.Payload)
                    if (!result.FunctionError) {
                        return payload
                    }

                    throw Object.freeze(new InvocationError(payload.errorType, payload.errorMessage))
                }

                throw Object.freeze(new InvocationError('InvocationStatusCode', `Invocation failed with status code: ${result.StatusCode}`))
            })
        }
    }
}

module.exports = {LambdaInvoker, InvocationError}
