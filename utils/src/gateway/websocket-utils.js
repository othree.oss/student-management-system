const {Try} = require('@othree.io/optional')

function ResponseHandler(apiGatewayManagementApiBuilder) {
    return Object.freeze({
        respond: async (state, event) => {
            console.log('Responding with event', state, event)
            return Try(async () => {
                if (!event.metadata || !event.metadata.connectionId || !event.metadata.endpoint) {
                    return false
                }
                await apiGatewayManagementApiBuilder(event.metadata.endpoint).postToConnection({
                    ConnectionId: event.metadata.connectionId,
                    Data: JSON.stringify({
                        event: event.type,
                        body: state,
                        transactionId: event.metadata.transactionId
                    })
                }).promise()
                return true
            })
        },
        notifyError: async (action, metadata, errors) => {
            console.log('Notifying error', action, metadata, errors)
            return Try(async () => {
                if (!metadata.connectionId || !metadata.endpoint) {
                    return false
                }
                await apiGatewayManagementApiBuilder(metadata.endpoint).postToConnection({
                    ConnectionId: metadata.connectionId,
                    Data: JSON.stringify({
                        action: action,
                        errors: errors,
                        transactionId: metadata.transactionId
                    })
                }).promise()
                return true
            })
        }
    })
}

module.exports = {ResponseHandler}
