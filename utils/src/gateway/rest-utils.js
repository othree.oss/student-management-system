const createResponse = (statusCode, responseBody) => (Object.freeze({
    statusCode,
    body: JSON.stringify(responseBody),
    headers: {'Access-Control-Allow-Origin': '*'}
}))

module.exports = {createResponse}
