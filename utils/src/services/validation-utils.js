const validateFn = require('validate.js')

class ValidationError extends Error {
    constructor(validationErrors) {
        super('Validation errors')

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, ValidationError)
        }

        this.errors = validationErrors
    }
}

module.exports = { ValidationError }

function validate(object, constraints) {
    const validation = validateFn(object, constraints)

    if (validation) {
        throw new ValidationError(validation)
    }
}

module.exports = {ValidationError, validate}
