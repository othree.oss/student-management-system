
function BaseProjectionService(baseProjectionRepository) {
    return Object.freeze({
        get: async (id) => {
            return baseProjectionRepository.get(id)
        },
        getAll: async () => {
            return baseProjectionRepository.getAll()
        }
    })
}

module.exports = {BaseProjectionService}