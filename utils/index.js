const {createResponse} = require('./src/gateway/rest-utils')
const {lambdaInvoker, InvocationError} = require('./src/lambda/lambda-invoker')
const {ResponseHandler} = require('./src/gateway/websocket-utils')
const {validate, ValidationError} = require('./src/services/validation-utils')
const {SqsCommandHandler} = require('./src/lambda/sqs-command-handler')
const {SqsUpdateProjectionHandler} = require('./src/lambda/sqs-update-projection-handler')
const {BaseProjectionRestHandlers} = require('./src/lambda/base-projection-rest-handlers')
const {BaseProjectionRepository} = require('./src/dynamodb/base-projection-repository')
const {BaseProjectionService} = require('./src/services/base-projection-service')

module.exports = {
    rest: {
        createResponse
    },
    lambda: {
        lambdaInvoker,
        InvocationError,
        SqsCommandHandler,
        SqsUpdateProjectionHandler,
        BaseProjectionRestHandlers
    },
    websockets: {
        ResponseHandler
    },
    validation: {
        validate,
        ValidationError
    },
    dynamodb: {
        BaseProjectionRepository
    },
    services: {
        BaseProjectionService
    }
}
