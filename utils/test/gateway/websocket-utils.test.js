const {ResponseHandler} = require('../../src/gateway/websocket-utils')

describe('ResponseHandler', () => {
    describe('respond', () => {
        it('should respond with the specified state and event data', async () => {
            const managementApiBuilder = (wsEndpoint) => {
                expect(wsEndpoint).toEqual('ws://localhost')
                return {
                    postToConnection: (params) => {
                        expect(params).toStrictEqual({
                            ConnectionId: 'c0nn3ct10n',
                            Data: JSON.stringify({
                                event: 'SomethingHappened',
                                body: {
                                    message: 'Hello World'
                                },
                                transactionId: '7r4ns4ct10n'
                            })
                        })

                        return {
                            promise: async () => {
                                return true
                            }
                        }
                    }
                }
            }

            const responseHandler = ResponseHandler(managementApiBuilder)

            const event = {
                type: 'SomethingHappened',
                body: {
                    message: 'Hello World'
                },
                metadata: {
                    connectionId: 'c0nn3ct10n',
                    endpoint: 'ws://localhost',
                    transactionId: '7r4ns4ct10n'
                }
            }

            const state = {
                message: 'Hello World'
            }

            const maybeResponse = await responseHandler.respond(state, event)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should return false if the event does not contain metadata', async () => {
            const responseHandler = ResponseHandler({})

            const event = {
                type: 'SomethingHappened',
                body: {
                    message: 'Hello World'
                }
            }

            const state = {
                message: 'Hello World'
            }

            const maybeResponse = await responseHandler.respond(state, event)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeFalsy()
        })

        it('should return false if the event metadata does not contain a connectionId', async () => {
            const responseHandler = ResponseHandler({})

            const event = {
                type: 'SomethingHappened',
                body: {
                    message: 'Hello World'
                },
                metadata: {}
            }

            const state = {
                message: 'Hello World'
            }

            const maybeResponse = await responseHandler.respond(state, event)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeFalsy()
        })

        it('should return false if the event metadata does not contain a wsEndpoint', async () => {
            const responseHandler = ResponseHandler({})

            const event = {
                type: 'SomethingHappened',
                body: {
                    message: 'Hello World'
                },
                metadata: {
                    connectionId: 'c0nn3ct10n'
                }
            }

            const state = {
                message: 'Hello World'
            }

            const maybeResponse = await responseHandler.respond(state, event)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeFalsy()
        })
    })

    describe('notifyError', () => {
        it('should notify the specified error', async () => {
            const managementApiBuilder = (wsEndpoint) => {
                expect(wsEndpoint).toEqual('ws://localhost')
                return {
                    postToConnection: (params) => {
                        expect(params).toStrictEqual({
                            ConnectionId: 'c0nn3ct10n',
                            Data: JSON.stringify({
                                action: 'createprofessor',
                                errors: {
                                    'name': ['name is required']
                                },
                                transactionId: '7r4ns4ct10n'
                            })
                        })

                        return {
                            promise: async () => {
                                return true
                            }
                        }
                    }
                }
            }

            const responseHandler = ResponseHandler(managementApiBuilder)

            const metadata = {
                connectionId: 'c0nn3ct10n',
                endpoint: 'ws://localhost',
                transactionId: '7r4ns4ct10n'
            }

            const errors = {
                'name': ['name is required']
            }

            const maybeResponse = await responseHandler.notifyError('createprofessor', metadata, errors)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeTruthy()
        })

        it('should return false if the event metadata does not contain a connectionId', async () => {
            const responseHandler = ResponseHandler({})

            const metadata = {}

            const errors = {
                'name': ['name is required']
            }

            const maybeResponse = await responseHandler.notifyError('createprofessor', metadata, errors)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeFalsy()
        })

        it('should return false if the event metadata does not contain a wsEndpoint', async () => {
            const responseHandler = ResponseHandler({})

            const metadata = {
                connectionId: 'c0nn3ct10n'
            }

            const errors = {
                'name': ['name is required']
            }

            const maybeResponse = await responseHandler.notifyError('createprofessor', metadata, errors)

            expect(maybeResponse.isPresent).toBeTruthy()
            expect(maybeResponse.get()).toBeFalsy()
        })
    })
})