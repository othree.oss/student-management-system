const {createResponse} = require('../../src/gateway/rest-utils')

describe('createResponse', () => {
    it('should return the response with the serialized body', () => {
        const response = createResponse(200, {message: 'Hello World!'})

        expect(response).toStrictEqual({
            statusCode: 200,
            body: JSON.stringify({message: 'Hello World!'}),
            headers: {'Access-Control-Allow-Origin': '*'}
        })
    })

    it('should return the response with no body', () => {
        const response = createResponse(404)

        expect(response).toStrictEqual({
            statusCode: 404,
            body: undefined,
            headers: {'Access-Control-Allow-Origin': '*'}
        })
    })
})