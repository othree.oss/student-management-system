const { ValidationError, validate } = require('../../src/services/validation-utils')

describe('ValidationError', () => {
    it('should create a ValidationError with the correct properties', () => {
        const error = new ValidationError([{field: ["this has an error"]}])

        expect(error.message).toEqual("Validation errors")
        expect(error.errors).toStrictEqual([
            {
                field: ["this has an error"]
            }
        ])
    })
})

describe('validate', () => {
    it('should validate an object', () => {
        const testObject = {
            name: 'Chuck',
            lastName: 'Norris'
        }

        const constraint = {
            name: {
                presence: {
                    allowEmpty: false
                },
                type: 'string'
            }
        }

        validate(testObject, constraint)
    })

    it('should validate an object and throw a ValidationError if invalid', () => {
        const testObject = {
            name: undefined,
            lastName: 'Norris'
        }

        const constraint = {
            name: {
                presence: {
                    allowEmpty: false
                },
                type: 'string'
            }
        }

        try {
            validate(testObject, constraint)
            //Next line should never be executed, exception must be thrown. Next line makes the test fail in case
            //it doesn't throw an error.
            expect(true).toEqual(false)
        } catch (e) {
            expect(e).toBeInstanceOf(ValidationError)
        }
    })
})
