const {BaseProjectionService} = require('../../src/services/base-projection-service')
const {Optional} = require('@othree.io/optional')

describe('BaseProjectionService', () => {
    describe('get', () => {
        it('should get the specified entity', async () => {
            const expectedProfessor = {
                id: '1337',
                name: 'Chuck Norris',
                rating: {
                    overallRating: 0,
                    stars: [0, 0, 0, 0, 0]
                }
            }

            const repository = {
                get: async (id) => {
                    expect(id).toEqual('1337')

                    return Optional({...expectedProfessor})
                }
            }

            const service = BaseProjectionService(repository)

            const maybeProfessor = await service.get('1337')

            expect(maybeProfessor.isPresent).toBeTruthy()
            expect(maybeProfessor.get()).toStrictEqual(expectedProfessor)
        })
    })

    describe('getAll', () => {
        it('should get all the entities', async () => {
            const expectedProfessors = [
                {
                    id: '1337',
                    name: 'Chuck Norris'
                },
                {
                    id: '7331',
                    name: 'Bruce Lee'
                }
            ]

            const repository = {
                getAll: async () => {
                    return Optional([...expectedProfessors])
                }
            }

            const service = BaseProjectionService(repository)

            const maybeProfessors = await service.getAll()

            expect(maybeProfessors.isPresent).toBeTruthy()
            expect(maybeProfessors.get()).toStrictEqual(expectedProfessors)
        })
    })
})