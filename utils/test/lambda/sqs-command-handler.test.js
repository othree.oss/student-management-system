const {SqsCommandHandler} = require('../../src/lambda/sqs-command-handler')
const {Optional} = require('@othree.io/optional')

describe('SqsCommandHandler', () => {

    it('should respond with each of the events', async () => {
        const professorAggregate = {
            handle: async (command) => {
                expect(command).toStrictEqual({
                    type: 'CreateProfessor',
                    body: {
                        name: 'Chuck Norris'
                    }
                })

                return {
                    state: {
                        id: '1337',
                        name: 'Chuck Norris',
                        rating: {
                            overallRating: 0,
                            stars: [0, 0, 0, 0, 0]
                        }
                    },
                    events: [],
                    newEvents: [
                        {
                            contextId: '1337',
                            type: 'ProfessorCreated',
                            body: {
                                name: 'Chuck Norris'
                            }
                        }
                    ]
                }
            }
        }

        const responseHandler = {
            respond: async (state, event) => {
                expect(state).toStrictEqual({
                    id: '1337',
                    name: 'Chuck Norris',
                    rating: {
                        overallRating: 0,
                        stars: [0, 0, 0, 0, 0]
                    }
                })
                expect(event).toStrictEqual({
                    contextId: '1337',
                    type: 'ProfessorCreated',
                    body: {
                        name: 'Chuck Norris'
                    }
                })

                return Optional(true)
            }
        }
        const handler = SqsCommandHandler(professorAggregate, responseHandler)

        const lambdaEvent = {
            Records: [
                {
                    body: JSON.stringify({
                        type: 'CreateProfessor',
                        body: {
                            name: 'Chuck Norris'
                        }
                    })
                }
            ]
        }

        const result = await handler.commandHandler(lambdaEvent)

        expect(result).toStrictEqual([true])
    })
})
