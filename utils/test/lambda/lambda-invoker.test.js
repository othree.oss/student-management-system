const {LambdaInvoker, InvocationError} = require('../../src/lambda/lambda-invoker')

describe('LambdaInvoker', () => {
    it('should invoke the specified function with the provided parameters', async () => {
        const lambda = {
            invoke: (params) => {
                expect(params).toStrictEqual({
                    FunctionName: 'arn:fn:testFn',
                    Payload: JSON.stringify({get: 'salute'})
                })

                return {
                    promise: () => {
                        return Promise.resolve({StatusCode: 200, Payload: JSON.stringify({hello: 'world'})})
                    }
                }
            }
        }
        const invoker = LambdaInvoker(lambda)
        const maybeResult = await invoker.invoke('arn:fn:testFn', {get: 'salute'})

        expect(maybeResult.isPresent).toBeTruthy()
        expect(maybeResult.get()).toStrictEqual({
            hello: 'world'
        })
    })

    it('should invoke the specified function and return an empty optional if response has an unexpected status code', async () => {
        const lambda = {
            invoke: (params) => {
                return {
                    promise: () => {
                        return Promise.resolve({StatusCode: 500, Payload: JSON.stringify({hello: 'world'})})
                    }
                }
            }
        }
        const invoker = LambdaInvoker(lambda)
        const maybeResult = await invoker.invoke('arn:fn:testFn', {get: 'salute'})

        expect(maybeResult.isPresent).toBeFalsy()
        expect(maybeResult.getError()).toStrictEqual(new InvocationError('InvocationStatusCode', 'Invocation failed with status code: 500'))
        expect(maybeResult.getError().errorType).toEqual('InvocationStatusCode')
    })

    it('should invoke the specified function and return an empty optional if response has an error', async () => {
        const lambda = {
            invoke: (params) => {
                return {
                    promise: () => {
                        return Promise.resolve({
                            StatusCode: 200,
                            FunctionError: 'Failure',
                            Payload: JSON.stringify({errorType: 'NotFound', errorMessage: 'It was not found'})
                        })
                    }
                }
            }
        }
        const invoker = LambdaInvoker(lambda)
        const maybeResult = await invoker.invoke('arn:fn:testFn', {get: 'salute'})

        expect(maybeResult.isPresent).toBeFalsy()
        expect(maybeResult.getError()).toStrictEqual(new InvocationError('NotFound', 'It was not found'))
        expect(maybeResult.getError().errorType).toEqual('NotFound')
    })

    it('should invoke the specified function and return an empty optional if the invocation fails', async () => {
        const lambda = {
            invoke: (params) => {
                return {
                    promise: () => {
                        return Promise.reject(new Error('KAPOW!'))
                    }
                }
            }
        }
        const invoker = LambdaInvoker(lambda)
        const maybeResult = await invoker.invoke('arn:fn:testFn', {get: 'salute'})

        expect(maybeResult.isPresent).toBeFalsy()
    })
})
