const {BaseProjectionRestHandlers} = require('../../src/lambda/base-projection-rest-handlers')
const {Optional} = require('@othree.io/optional')
const {createResponse} = require('utils').rest

describe('BaseProjectionRestHandlers', () => {
    describe('getHandler', () => {
        it('should return 200 and the entity', async () => {
            const service = {
                get: async (id) => {
                    expect(id).toEqual('1337')
                    return Optional({
                        id: '1337',
                        name: 'Chuck Norris',
                        rating: {
                            overallRating: 0,
                            stars: [0, 0, 0, 0, 0]
                        }
                    })
                }
            }
            const handlers = BaseProjectionRestHandlers(service)

            const lambdaEvent = {
                pathParameters: {
                    id: '1337'
                }
            }
            const response = await handlers.getHandler(lambdaEvent)

            const expectedProfessor = {
                id: '1337',
                name: 'Chuck Norris',
                rating: {
                    overallRating: 0,
                    stars: [0, 0, 0, 0, 0]
                }
            }

            expect(response).toStrictEqual(createResponse(200, expectedProfessor))
        })

        it('should return 404 if the entity does not exist', async () => {
            const service = {
                get: async (id) => {
                    return Optional()
                }
            }
            const handlers = BaseProjectionRestHandlers(service)

            const lambdaEvent = {
                pathParameters: {
                    id: '1337'
                }
            }

            const response = await handlers.getHandler(lambdaEvent)

            expect(response).toStrictEqual(createResponse(404))
        })
    })

    describe('getAllHandler', () => {
        it('should return 200 and the entities', async () => {
            const expectedProfessors = [
                {
                    id: '1337',
                    name: 'Chuck Norris',
                    rating: {
                        overallRating: 0,
                        stars: [0, 0, 0, 0, 0]
                    }
                },
                {
                    id: '7331',
                    name: 'Bruce Lee',
                    rating: {
                        overallRating: 0,
                        stars: [0, 0, 0, 0, 0]
                    }
                }
            ]

            const service = {
                getAll: async () => {
                    return Optional([...expectedProfessors])
                }
            }
            const handlers = BaseProjectionRestHandlers(service)

            const response = await handlers.getAllHandler({})

            expect(response).toStrictEqual(createResponse(200, expectedProfessors))
        })

        it('should return 500 if getting all entities fails', async () => {
            const service = {
                getAll: async () => {
                    return Optional()
                }
            }
            const handlers = BaseProjectionRestHandlers(service)

            const response = await handlers.getAllHandler({})

            expect(response).toStrictEqual(createResponse(500))
        })
    })
})
