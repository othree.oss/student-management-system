const {SqsUpdateProjectionHandler} = require('../../src/lambda/sqs-update-projection-handler')
const {Optional} = require('@othree.io/optional')

describe('SqsUpdateProjectionHandler', () => {
    describe('updateProjectionHandler', () => {
        it('should return an array with the updated entities', async () => {
            const expectedEvent = {
                contextId: '1337',
                type: 'ProfessorCreated',
                body: {
                    name: 'Chuck Norris'
                }
            }
            const expectedState = {
                id: '1337',
                name: 'Chuck Norris',
                rating: {
                    overallRating: 0,
                    stars: [0, 0, 0, 0, 0]
                }
            }
            const service = {
                updateProjection: async (event, state) => {
                    expect(event).toStrictEqual(expectedEvent)
                    expect(state).toStrictEqual(expectedState)

                    return Optional({
                        id: '1337',
                        name: 'Chuck Norris',
                        rating: {
                            overallRating: 0,
                            stars: [0, 0, 0, 0, 0]
                        }
                    })
                }
            }

            const sqsEvent = {
                Records: [
                    {
                        body: JSON.stringify({
                            Message: JSON.stringify({
                                state: expectedState,
                                event: expectedEvent
                            })
                        })
                    }
                ]
            }
            const handlers = SqsUpdateProjectionHandler(service)

            const updateResults = await handlers.updateProjectionHandler(sqsEvent)

            expect(updateResults).toStrictEqual([{...expectedState}])
        })

        it('should return an array with undefined if updating the projection fails', async () => {
            const expectedEvent = {
                contextId: '1337',
                type: 'ProfessorCreated',
                body: {
                    name: 'Chuck Norris'
                }
            }
            const expectedState = {
                id: '1337',
                name: 'Chuck Norris',
                rating: {
                    overallRating: 0,
                    stars: [0, 0, 0, 0, 0]
                }
            }
            const service = {
                updateProjection: async (event, state) => {
                    return Optional()
                }
            }

            const sqsEvent = {
                Records: [
                    {
                        body: JSON.stringify({
                            Message: JSON.stringify({
                                state: expectedState,
                                event: expectedEvent
                            })
                        })
                    }
                ]
            }
            const handlers = SqsUpdateProjectionHandler(service)

            const updateResults = await handlers.updateProjectionHandler(sqsEvent)

            expect(updateResults).toStrictEqual([undefined])
        })
    })
})