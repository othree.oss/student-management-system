const {BaseProjectionRepository} = require('../../src/dynamodb/base-projection-repository')

describe('BaseProjectionRepository', () => {
    const configuration = {
        ProjectionTable: 'ProfessorProjection'
    }

    describe('upsert', () => {
        it('should insert or update the specified entity', async () => {
            const professor = {
                id: '1337',
                name: 'Chuck Norris'
            }
            const documentClient = {
                put: (params) => {
                    expect(params).toStrictEqual({
                        TableName: configuration.ProjectionTable,
                        Item: {
                            ...professor
                        },
                        ReturnValues: 'NONE',
                        ReturnConsumedCapacity: 'NONE',
                        ReturnItemCollectionMetrics: 'NONE'
                    })

                    return {
                        promise: async () => {
                            return true
                        }
                    }
                }
            }

            const repository = BaseProjectionRepository(documentClient, configuration)

            const maybeEntity = await repository.upsert(professor)

            expect(maybeEntity.isPresent).toBeTruthy()
            expect(maybeEntity.get()).toStrictEqual(professor)
        })
    })

    describe('get', () => {
        it('should get the specified entity', async () => {
            const documentClient = {
                get: (params) => {
                    expect(params).toStrictEqual({
                        TableName: configuration.ProjectionTable,
                        Key: {
                            id: '1337'
                        }
                    })

                    return {
                        promise: async () => {
                            return {
                                Item: {
                                    id: '1337',
                                    name: 'Chuck Norris'
                                }
                            }
                        }
                    }
                }
            }
            const repository = BaseProjectionRepository(documentClient, configuration)

            const maybeEntity = await repository.get('1337')

            expect(maybeEntity.isPresent).toBeTruthy()
            expect(maybeEntity.get()).toStrictEqual({
                id: '1337',
                name: 'Chuck Norris'
            })
        })
    })

    describe('getAll', () => {
        it('should get all entities', async () => {
            const documentClient = {
                scan: (params) => {
                    expect(params).toStrictEqual({
                        TableName: configuration.ProjectionTable
                    })

                    return {
                        promise: async () => {
                            return {
                                Items: [
                                    {
                                        id: '1337',
                                        name: 'Chuck Norris'
                                    },
                                    {
                                        id: '7331',
                                        name: 'Bruce Lee'
                                    }
                                ]
                            }
                        }
                    }
                }
            }
            const repository = BaseProjectionRepository(documentClient, configuration)

            const maybeEntities = await repository.getAll()

            expect(maybeEntities.isPresent).toBeTruthy()
            expect(maybeEntities.get()).toStrictEqual([
                {
                    id: '1337',
                    name: 'Chuck Norris'
                },
                {
                    id: '7331',
                    name: 'Bruce Lee'
                }
            ])
        })
    })
})
