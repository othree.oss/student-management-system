#!/bin/bash
BASE_DIR=`pwd`

deploy() {
  cd $1/codebase
  npm install
  cd ../infrastructure
  npm install
  npm run cdk -- --profile othree -c env=Dev deploy '*'
  cd $BASE_DIR
}

cd utils
npm install
cd $BASE_DIR

echo '---------- DEPLOYING PROFESSOR MICROSERVICE ----------'
deploy 'professor'

echo '---------- DEPLOYING STUDENT MICROSERVICE ----------'
deploy 'student'

echo '---------- DEPLOYING CLUB MICROSERVICE ----------'
deploy 'club'

echo '---------- DEPLOYING CLUB MEMBERSHIP MICROSERVICE ----------'
deploy 'club-membership'

echo '---------- DEPLOYING GATEWAY ----------'
deploy 'gateway'