#!/usr/bin/env node
const cdk = require('@aws-cdk/core')
const {ClubMembershipCoreStack} = require('../lib/club-membership-core-stack')
const {ClubMembershipWriteStack} = require('../lib/club-membership-write-stack')
const {ClubMembershipReadStack} = require('../lib/club-membership-read-stack')

const app = new cdk.App();
const env = app.node.tryGetContext('env')

new ClubMembershipCoreStack(env, app, `Club-Membership-Core-Stack-${env}`)
new ClubMembershipWriteStack(env, app, `Club-Membership-Write-Stack-${env}`)
new ClubMembershipReadStack(env, app, `Club-Membership-Read-Stack-${env}`)
