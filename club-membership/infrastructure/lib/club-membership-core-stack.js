const cdk = require('@aws-cdk/core');
const sns = require('@aws-cdk/aws-sns');

class ClubMembershipCoreStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props);

        const topic = new sns.Topic(this, `Club-Membership-Topic-${env}`, {
            topicName: `Club-Membership-Topic-${env}`,
            displayName: 'Club-Membership Topic',
            fifo: true,
            contentBasedDeduplication: true
        })

        new cdk.CfnOutput(this, `Output-Club-Membership-Topic-${env}`, {
            value: topic.topicArn,
            exportName: `Club-Membership-Topic-${env}`
        })
    }
}

module.exports = {ClubMembershipCoreStack}
