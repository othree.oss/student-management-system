const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const iam = require('@aws-cdk/aws-iam')

class ClubMembershipWriteStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const clubMembershipTopicARN = cdk.Fn.importValue(`Club-Membership-Topic-${env}`)
        const clubMembershipTopic = sns.Topic.fromTopicArn(this, `Club-Membership-Topic-${env}`, clubMembershipTopicARN)

        const clubMembershipEventsTable = new dynamodb.Table(this, `Club-Membership-Events-${env}`, {
            tableName: `Club-Membership-Events-${env}`,
            partitionKey: {name: 'contextId', type: dynamodb.AttributeType.STRING},
            sortKey: {name: 'eventDate', type: dynamodb.AttributeType.NUMBER},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST
        })

        const clubMembershipCommandsDLQ = new sqs.Queue(this, `Club-Membership-SQS-CommandsDLQ-${env}`, {
            queueName: `Club-Membership-SQS-CommandsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const clubMembershipCommandsQ = new sqs.Queue(this, `Club-Membership-SQS-Commands-${env}`, {
            queueName: `Club-Membership-SQS-Commands-${env}.fifo`,
            deadLetterQueue: {
                queue: clubMembershipCommandsDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        const commandHandler = new lambda.Function(this, `Club-Membership-SqsFn-CommandHandler-${env}`, {
            functionName: `Club-Membership-SqsFn-CommandHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/write/entrypoint.sqs.commandHandler',
            environment: {
                CLUB_MEMBERSHIP_EVENTS_TABLE: clubMembershipEventsTable.tableName,
                CLUB_MEMBERSHIP_EVENTS_TOPIC: clubMembershipTopicARN
            }
        })
        clubMembershipEventsTable.grantReadWriteData(commandHandler)
        clubMembershipTopic.grantPublish(commandHandler)

        commandHandler.addEventSource(
            new SqsEventSource(
                clubMembershipCommandsQ
            )
        )

        commandHandler.addToRolePolicy(new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: ['execute-api:Invoke', 'execute-api:ManageConnections'],
            resources: ['arn:aws:execute-api:*:*:*']
        }))

        new cdk.CfnOutput(this, `Export-Club-Membership-SQS-Commands-${env}`, {
            exportName: `Club-Membership-SQS-Commands-${env}`,
            value: clubMembershipCommandsQ.queueArn
        })

        new cdk.CfnOutput(this, `Export-Club-Membership-SqsFn-CommandHandler-${env}`, {
            exportName: `Club-Membership-SqsFn-CommandHandler-${env}`,
            value: commandHandler.functionArn
        })
    }
}

module.exports = {ClubMembershipWriteStack}
