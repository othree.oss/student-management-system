const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const subs = require('@aws-cdk/aws-sns-subscriptions')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const iam = require('@aws-cdk/aws-iam')

class ClubMembershipReadStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const clubMembershipTopicARN = cdk.Fn.importValue(`Club-Membership-Topic-${env}`)
        const clubMembershipTopic = sns.Topic.fromTopicArn(this, `Club-Membership-Topic-${env}`, clubMembershipTopicARN)

        const clubMembershipDLQ = new sqs.Queue(this, `Club-Membership-SQS-EventsDLQ-${env}`, {
            queueName: `Club-Membership-SQS-EventsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const clubMembershipEventsQ = new sqs.Queue(this, `Club-Membership-SQS-Events-${env}`, {
            queueName: `Club-Membership-SQS-Events-${env}.fifo`,
            deadLetterQueue: {
                queue: clubMembershipDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        clubMembershipTopic.addSubscription(
            new subs.SqsSubscription(
                clubMembershipEventsQ,
                {
                    filterPolicy: {
                        bc: sns.SubscriptionFilter.stringFilter({
                            whitelist: ['club-membership']
                        })
                    }
                }
            )
        )

        const clubMembershipProjectionTable = new dynamodb.Table(this, `Club-Membership-Projection-${env}`, {
            tableName: `Club-Membership-Projection-${env}`,
            partitionKey: {name: 'id', type: dynamodb.AttributeType.STRING},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        })

        const projectionIndexName = 'ClubMembershipClubStudentIndex'

        clubMembershipProjectionTable.addGlobalSecondaryIndex({
            indexName: projectionIndexName,
            partitionKey: {name: 'clubId', type: dynamodb.AttributeType.STRING},
            sortKey: {name: 'studentId', type: dynamodb.AttributeType.STRING},
            projectionType: dynamodb.ProjectionType.ALL
        })

        const updateProjectionHandler = new lambda.Function(this, `Club-Membership-SqsFn-UpdateProjectionHandler-${env}`, {
            functionName: `Club-Membership-SqsFn-UpdateProjectionHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.sqs.updateProjectionHandler',
            environment: {
                CLUB_MEMBERSHIP_PROJECTION_TABLE: clubMembershipProjectionTable.tableName,
                CLUB_MEMBERSHIP_CLUB_STUDENT_INDEX: projectionIndexName
            }
        })
        clubMembershipProjectionTable.grantReadWriteData(updateProjectionHandler)

        updateProjectionHandler.addEventSource(
            new SqsEventSource(
                clubMembershipEventsQ
            )
        )

        const getClubMembershipHandler = new lambda.Function(this, `Club-Membership-RestFn-GetClubMembershipHandler-${env}`, {
            functionName: `Club-Membership-RestFn-GetClubMembershipHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getHandler',
            environment: {
                CLUB_MEMBERSHIP_PROJECTION_TABLE: clubMembershipProjectionTable.tableName,
                CLUB_MEMBERSHIP_CLUB_STUDENT_INDEX: projectionIndexName
            }
        })
        clubMembershipProjectionTable.grantReadData(getClubMembershipHandler)
        getClubMembershipHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        const getAllClubMembershipsHandler = new lambda.Function(this, `Club-Membership-RestFn-GetAllClubMembershipsHandler-${env}`, {
            functionName: `Club-Membership-RestFn-GetAllClubMembershipsHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getAllHandler',
            environment: {
                CLUB_MEMBERSHIP_PROJECTION_TABLE: clubMembershipProjectionTable.tableName,
                CLUB_MEMBERSHIP_CLUB_STUDENT_INDEX: projectionIndexName
            }
        })
        clubMembershipProjectionTable.grantReadData(getAllClubMembershipsHandler)
        getAllClubMembershipsHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        const getByClubAndStudentHandler = new lambda.Function(this, `Club-Membership-RestFn-GetByClubAndStudentHandler-${env}`, {
            functionName: `Club-Membership-RestFn-GetByClubAndStudentHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getByClubAndStudentHandler',
            environment: {
                CLUB_MEMBERSHIP_PROJECTION_TABLE: clubMembershipProjectionTable.tableName,
                CLUB_MEMBERSHIP_CLUB_STUDENT_INDEX: projectionIndexName
            }
        })
        clubMembershipProjectionTable.grantReadData(getByClubAndStudentHandler)
        getByClubAndStudentHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        new cdk.CfnOutput(this, `Output-Club-Membership-RestFn-GetClubMembershipHandler-${env}`, {
            exportName: `Club-Membership-RestFn-GetClubMembershipHandler-${env}`,
            value: getClubMembershipHandler.functionArn
        })

        new cdk.CfnOutput(this, `Output-Club-Membership-RestFn-GetAllClubMembershipsHandler-${env}`, {
            exportName: `Club-Membership-RestFn-GetAllClubMembershipsHandler-${env}`,
            value: getAllClubMembershipsHandler.functionArn
        })

        new cdk.CfnOutput(this, `Output-Club-Membership-RestFn-GetByClubAndStudentHandler-${env}`, {
            exportName: `Club-Membership-RestFn-GetByClubAndStudentHandler-${env}`,
            value: getByClubAndStudentHandler.functionArn
        })
    }
}

module.exports = {ClubMembershipReadStack}
