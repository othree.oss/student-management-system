const {ClubMembershipHandlers} = require('../../../src/read/handlers/club-membership-handlers')
const {createResponse} = require('utils').rest
const {Optional} = require('@othree.io/optional')

describe('ClubMembershipHandlers', () => {
    describe('getByClubAndStudentHandler', () => {
        it('should return 200 and the requested membership', async () => {
            const expectedMembership = {
                id: '1337',
                studentId: 'abc',
                clubId: '123',
                status: false,
                assignments: [],
                grade: 0
            }
            const service = {
                getByClubAndStudent: async (clubId, studentId) => {
                    expect(clubId).toEqual('123')
                    expect(studentId).toEqual('abc')
                    return Optional({
                        ...expectedMembership
                    })
                }
            }
            const handlers = ClubMembershipHandlers(service)

            const lambdaEvent = {
                pathParameters: {
                    id: '123',
                    studentId: 'abc'
                }
            }
            const response = await handlers.getByClubAndStudentHandler(lambdaEvent)

            expect(response).toStrictEqual(createResponse(200, expectedMembership))
        })

        it('should return 404 if the membership si not found', async () => {
            const service = {
                getByClubAndStudent: async (clubId, studentId) => {
                    return Optional()
                }
            }
            const handlers = ClubMembershipHandlers(service)

            const lambdaEvent = {
                pathParameters: {
                    id: '123',
                    studentId: 'abc'
                }
            }
            const response = await handlers.getByClubAndStudentHandler(lambdaEvent)

            expect(response).toStrictEqual(createResponse(404))
        })
    })
})