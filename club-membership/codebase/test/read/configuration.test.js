describe('Configuration', () => {
    process.env.CLUB_MEMBERSHIP_PROJECTION_TABLE = 'aws:arn:ClubMembershipProjectionTable'
    process.env.CLUB_MEMBERSHIP_CLUB_STUDENT_INDEX = 'ClubStudentIndex'

    const {Configuration} = require('../../src/read/configuration')

    it('should return the configured ProjectionTable', () => {
        expect(Configuration.ProjectionTable).toEqual('aws:arn:ClubMembershipProjectionTable')
    })

    it('should return the configured ClubMembershipClubStudentIndex', () => {
        expect(Configuration.ClubMembershipClubStudentIndex).toEqual('ClubStudentIndex')
    })
})
