const {ClubMembershipService} = require('../../../src/read/services/club-membership-service')
const {EVENT_TYPES} = require('../../../src/shared/event-types')
const {Optional} = require('@othree.io/optional')

describe('ClubMembershipService', () => {
    describe('updateProjection', () => {

        const updateEvents = [EVENT_TYPES.STUDENT_ENROLLED, EVENT_TYPES.GRADED_ASSIGNMENT_ADDED, EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED]

        updateEvents.forEach(eventType => {
            it(`should update the projection when the event is ${eventType}`, async () => {
                const state = {
                    id: '1337',
                    studentId: 'abc',
                    clubId: '123',
                    status: false,
                    assignments: [],
                    grade: 0
                }

                const repository = {
                    upsert: async (clubMembership) => {
                        expect(clubMembership).toStrictEqual(state)

                        return Optional({...clubMembership})
                    }
                }

                const service = ClubMembershipService(repository)

                const maybeClubMembership = await service.updateProjection({type: eventType}, state)

                expect(maybeClubMembership.isPresent).toBeTruthy()
                expect(maybeClubMembership.get()).toStrictEqual(state)
            })
        })

        it('should return an empty optional if the event is unhandled', async () => {
            const state = {
                id: 'abc'
            }

            const service = ClubMembershipService({})

            const event = {
                type: 'UNEXPECTED'
            }

            const maybeClubMembership = await service.updateProjection(event, state)

            expect(maybeClubMembership.isPresent).toBeFalsy()
        })

    })

    describe('getByClubAndStudent', () => {
        it('should get the membership using the specified club and student', async () => {
            const expectedMembership = {
                id: '1337',
                studentId: 'abc',
                clubId: '123',
                status: false,
                assignments: [],
                grade: 0
            }
            const repository = {
                getByClubAndStudent: async (clubId, studentId) => {
                    expect(clubId).toEqual('123')
                    expect(studentId).toEqual('abc')
                    return Optional({
                        ...expectedMembership
                    })
                }
            }
            const service = ClubMembershipService(repository)

            const maybeMembership = await service.getByClubAndStudent('123', 'abc')

            expect(maybeMembership.isPresent).toBeTruthy()
            expect(maybeMembership.get()).toStrictEqual(expectedMembership)
        })
    })
})