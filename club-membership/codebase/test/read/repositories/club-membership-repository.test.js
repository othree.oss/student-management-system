const {ClubMembershipRepository} = require('../../../src/read/repositories/club-membership-repository')

describe('ClubMembershipRepository', () => {
    const configuration = {
        ProjectionTable: 'aws:arn:ClubMembershipProjection',
        ClubMembershipClubStudentIndex: 'ClubMembershipClubStudentIndex'
    }
    describe('getByClubAndStudent', () => {
        it('should get the membership using the specified club and student', async () => {
            const expectedMembership = {
                id: '1337',
                studentId: 'abc',
                clubId: '123',
                status: false,
                assignments: [],
                grade: 0
            }
            const documentClient = {
                query: (params) => {
                    expect(params).toStrictEqual({
                        TableName: configuration.ProjectionTable,
                        IndexName: configuration.ClubMembershipClubStudentIndex,
                        KeyConditionExpression: '#clubId = :clubId and #studentId = :studentId',
                        ExpressionAttributeNames: {
                            '#clubId': 'clubId',
                            '#studentId': 'studentId'
                        },
                        ExpressionAttributeValues: {
                            ':clubId': '123',
                            ':studentId': 'abc'
                        }
                    })
                    return {
                        promise: async () => {
                            return {
                                Count: 1,
                                Items: [{...expectedMembership}]
                            }
                        }
                    }
                }
            }
            const repository = ClubMembershipRepository(configuration, documentClient)

            const maybeMembership = await repository.getByClubAndStudent('123', 'abc')

            expect(maybeMembership.isPresent).toBeTruthy()
            expect(maybeMembership.get()).toStrictEqual(expectedMembership)
        })

        it('should return an empty optional if the membership is not found for the specified club and student', async () => {
            const expectedMembership = {
                id: '1337',
                studentId: 'abc',
                clubId: '123',
                status: false,
                assignments: [],
                grade: 0
            }
            const documentClient = {
                query: (params) => {
                    expect(params).toStrictEqual({
                        TableName: configuration.ProjectionTable,
                        IndexName: configuration.ClubMembershipClubStudentIndex,
                        KeyConditionExpression: '#clubId = :clubId and #studentId = :studentId',
                        ExpressionAttributeNames: {
                            '#clubId': 'clubId',
                            '#studentId': 'studentId'
                        },
                        ExpressionAttributeValues: {
                            ':clubId': '123',
                            ':studentId': 'abc'
                        }
                    })
                    return {
                        promise: async () => {
                            return {
                                Count: 0,
                                Items: []
                            }
                        }
                    }
                }
            }
            const repository = ClubMembershipRepository(configuration, documentClient)

            const maybeMembership = await repository.getByClubAndStudent('123', 'abc')

            expect(maybeMembership.isPresent).toBeFalsy()
        })
    })
})