const {ClubMembershipCommandHandler} = require('../../../src/write/commands/club-membership-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('AddGradedAssignment', () => {
    const uuid= () => 'phys1c5'

    it('should return a GRADED_ASSIGNMENT_ADDED event', async () => {
        const handler = ClubMembershipCommandHandler(uuid)

        const command = {
            contextId: '1337',
            type: COMMAND_TYPES.ADD_GRADED_ASSIGNMENT,
            body: {
                assignmentId: 'phys1c5',
                marksObtained: 7,
                totalMarks: 10
            }
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.GRADED_ASSIGNMENT_ADDED,
            body: {
                assignmentId: 'phys1c5',
                marksObtained: 7,
                totalMarks: 10
            }
        })
    })
})
