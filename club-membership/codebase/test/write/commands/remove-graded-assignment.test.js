const {ClubMembershipCommandHandler} = require('../../../src/write/commands/club-membership-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('RemoveGradedAssignment', () => {

    it('should return a GRADED_ASSIGNMENT_ADDED event', async () => {
        const handler = ClubMembershipCommandHandler({})

        const command = {
            contextId: '1337',
            type: COMMAND_TYPES.REMOVE_GRADED_ASSIGNMENT,
            body: {
                assignmentId: 'phys1c5'
            }
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED,
            body: {
                assignmentId: 'phys1c5'
            }
        })
    })
})
