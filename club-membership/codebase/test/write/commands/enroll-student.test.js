const {ClubMembershipCommandHandler} = require('../../../src/write/commands/club-membership-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('EnrollStudent', () => {
    const uuid= () => '1337'

    it('should return a STUDENT_ENROLLED event', async () => {
        const handler = ClubMembershipCommandHandler(uuid)

        const command = {
            type: COMMAND_TYPES.ENROLL_STUDENT,
            body: {
                studentId: 'abc',
                clubId: '123'
            }
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.STUDENT_ENROLLED,
            body: {
                studentId: 'abc',
                clubId: '123'
            }
        })
    })
})