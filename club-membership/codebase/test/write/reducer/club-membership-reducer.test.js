const {InitialState, reducer} = require('../../../src/write/reducer/club-membership-reducer')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('InitialState', () => {
    it('should be initialized correctly', () => {
        expect(InitialState).toStrictEqual({
            status: false,
            assignments: [],
            grade: 0
        })
    })
})

describe('reducer', () => {
    it('should handle a STUDENT_ENROLLED event', () => {
        const event = {
            contextId: '1337',
            type: EVENT_TYPES.STUDENT_ENROLLED,
            body: {
                studentId: 'abc',
                clubId: '123'
            }
        }

        const state = reducer(InitialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: false,
            assignments: [],
            grade: 0
        })
    })

    it('should handle a GRADED_ASSIGNMENT_ADDED event', () => {
        const initialState = {
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: false,
            assignments: [],
            grade: 0
        }

        const event = {
            contextId: '1337',
            type: EVENT_TYPES.GRADED_ASSIGNMENT_ADDED,
            body: {
                assignmentId: 'phys1c5',
                marksObtained: 7,
                totalMarks: 10
            }
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: true,
            assignments: [
                {
                    assignmentId: 'phys1c5',
                    marksObtained: 7,
                    totalMarks: 10,
                    grade: 70
                }
            ],
            grade: 70
        })
    })

    it('should handle a GRADED_ASSIGNMENT_ADDED event and set the status to false if below threshold', () => {
        const initialState = {
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: true,
            assignments: [
                {
                    assignmentId: 'phys1c5',
                    marksObtained: 7,
                    totalMarks: 10,
                    grade: 70
                }
            ],
            grade: 70
        }

        const event = {
            contextId: '1337',
            type: EVENT_TYPES.GRADED_ASSIGNMENT_ADDED,
            body: {
                assignmentId: 'phys1c52',
                marksObtained: 2,
                totalMarks: 10
            }
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: false,
            assignments: [
                {
                    assignmentId: 'phys1c5',
                    marksObtained: 7,
                    totalMarks: 10,
                    grade: 70
                },
                {
                    assignmentId: 'phys1c52',
                    marksObtained: 2,
                    totalMarks: 10,
                    grade: 20
                }
            ],
            grade: 45
        })
    })

    it('should handle a GRADED_ASSIGNMENT_REMOVED event', () => {
        const initialState = {
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: false,
            assignments: [
                {
                    assignmentId: 'phys1c5',
                    marksObtained: 7,
                    totalMarks: 10,
                    grade: 70
                },
                {
                    assignmentId: 'phys1c52',
                    marksObtained: 2,
                    totalMarks: 10,
                    grade: 20
                }
            ],
            grade: 45
        }

        const event = {
            contextId: '1337',
            type: EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED,
            body: {
                assignmentId: 'phys1c52'
            }
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: true,
            assignments: [
                {
                    assignmentId: 'phys1c5',
                    marksObtained: 7,
                    totalMarks: 10,
                    grade: 70
                }
            ],
            grade: 70
        })
    })

    it('should handle a GRADED_ASSIGNMENT_REMOVED event and set status to false and grade to 0 if non left', () => {
        const initialState = {
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: true,
            assignments: [
                {
                    assignmentId: 'phys1c5',
                    marksObtained: 7,
                    totalMarks: 10,
                    grade: 70
                }
            ],
            grade: 70
        }

        const event = {
            contextId: '1337',
            type: EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED,
            body: {
                assignmentId: 'phys1c5'
            }
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            studentId: 'abc',
            clubId: '123',
            status: false,
            assignments: [],
            grade: 0
        })
    })
})