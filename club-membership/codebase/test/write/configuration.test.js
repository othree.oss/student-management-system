describe('Configuration', () => {
    process.env.CLUB_MEMBERSHIP_EVENTS_TABLE = 'aws:arn:ClubMembershipEventsTable'
    process.env.CLUB_MEMBERSHIP_EVENTS_TOPIC = 'aws:arn:ClubMembershipEventsTopic'

    const {Configuration} = require('../../src/write/configuration')

    it('should return the configured EventsTopic', () => {
        expect(Configuration.EventsTopic).toEqual('aws:arn:ClubMembershipEventsTopic')
    })

    it('should return the configured ClubMembershipEventsTable', () => {
        expect(Configuration.ClubMembershipEventsTable).toEqual('aws:arn:ClubMembershipEventsTable')
    })
})
