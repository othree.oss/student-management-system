const COMMAND_TYPES = Object.freeze({
    ENROLL_STUDENT: 'EnrollStudent',
    ADD_GRADED_ASSIGNMENT: 'AddGradedAssignment',
    REMOVE_GRADED_ASSIGNMENT: 'RemoveGradedAssignment'
})

module.exports = {COMMAND_TYPES}
