const {createEvent} = require('@othree.io/chisel')
const {EVENT_TYPES} = require('../../shared/event-types')

function RemoveGradedAssignment() {
    return Object.freeze({
        handle: async (state, command) => {
            const {body: assignment} = command

            return createEvent(
                command.contextId,
                EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED,
                {
                    assignmentId: assignment.assignmentId
                }
            )
        }
    })
}

module.exports = {RemoveGradedAssignment}
