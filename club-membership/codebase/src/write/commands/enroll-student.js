const {createEvent} = require('@othree.io/chisel')
const {EVENT_TYPES} = require('../../shared/event-types')

function EnrollStudent(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            const {body: membership} = command

            const membershipId = uuid()

            return createEvent(
                membershipId,
                EVENT_TYPES.STUDENT_ENROLLED,
                {
                    studentId: membership.studentId,
                    clubId: membership.clubId
                }
            )
        }
    })
}

module.exports = {EnrollStudent}
