const {createEvent} = require('@othree.io/chisel')
const {EVENT_TYPES} = require('../../shared/event-types')

function AddGradedAssignment(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            const {body: assignment} = command

            const assignmentId = uuid()

            return createEvent(
                command.contextId,
                EVENT_TYPES.GRADED_ASSIGNMENT_ADDED,
                {
                    assignmentId: assignmentId,
                    marksObtained: assignment.marksObtained,
                    totalMarks: assignment.totalMarks
                }
            )
        }
    })
}

module.exports = {AddGradedAssignment}
