const {COMMAND_TYPES} = require('./command-types')
const {AddGradedAssignment} = require('./add-graded-assignment')
const {EnrollStudent} = require('./enroll-student')
const {RemoveGradedAssignment} = require('./remove-graded-assignment')

function ClubMembershipCommandHandler(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Handling command', command, 'Current state', state)
            switch (command.type) {
                case COMMAND_TYPES.ENROLL_STUDENT:
                    const enrollStudent = EnrollStudent(uuid)
                    return enrollStudent.handle(state, command)
                case COMMAND_TYPES.ADD_GRADED_ASSIGNMENT:
                    const addGradedAssignment = AddGradedAssignment(uuid)
                    return addGradedAssignment.handle(state, command)
                case COMMAND_TYPES.REMOVE_GRADED_ASSIGNMENT:
                    const removeGradedAssignment = RemoveGradedAssignment()
                    return removeGradedAssignment.handle(state, command)
            }
        }
    })
}

module.exports = {ClubMembershipCommandHandler}
