const Configuration = Object.freeze({
    ClubMembershipEventsTable: process.env.CLUB_MEMBERSHIP_EVENTS_TABLE,
    EventsTopic: process.env.CLUB_MEMBERSHIP_EVENTS_TOPIC
})

module.exports = {Configuration}
