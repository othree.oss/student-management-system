const {EVENT_TYPES} = require('../../shared/event-types')
const currency = require('currency.js')

const MIN_PASSING_GRADE = 70

const InitialState = {
    status: false,
    assignments: [],
    grade: 0
}

const reducer = (state, event) => {
    switch (event.type) {
        case EVENT_TYPES.STUDENT_ENROLLED:
            return studentEnrolled(state, event)
        case EVENT_TYPES.GRADED_ASSIGNMENT_ADDED:
            return gradedAssignmentAdded(state, event)
        case EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED:
            return gradedAssignmentRemoved(state, event)
    }
}

const studentEnrolled = (state, event) => {
    const {body: membership} = event
    return {
        ...state,
        id: event.contextId,
        studentId: membership.studentId,
        clubId: membership.clubId
    }
}

const gradedAssignmentAdded = (state, event) => {
    const {body: assignment} = event
    const gradedAssignment = {
        ...assignment,
        grade: currency((assignment.marksObtained / assignment.totalMarks) * 100, {precision: 2}).value
    }
    const assignments = [...state.assignments, gradedAssignment]
    const totalScore = assignments.map(_ => _.grade).reduce((acum, value) => acum + value, 0)
    const grade = currency(totalScore / assignments.length, {precision: 2}).value

    return {
        ...state,
        assignments: assignments,
        grade: grade,
        status: grade >= MIN_PASSING_GRADE
    }
}

const gradedAssignmentRemoved = (state, event) => {
    const {body: assignment} = event
    const assignments = [...state.assignments]
    const index = state.assignments.findIndex(_ => _.assignmentId === assignment.assignmentId)
    if (index >= 0) {
        assignments.splice(index, 1)
    }
    const totalScore = assignments.map(_ => _.grade).reduce((acum, value) => acum + value, 0)
    const grade = assignments.length === 0 ? 0 : currency(totalScore / assignments.length, {precision: 2}).value

    return {
        ...state,
        assignments: assignments,
        grade: grade,
        status: grade >= MIN_PASSING_GRADE
    }
}

module.exports = {reducer, InitialState}