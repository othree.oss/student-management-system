const {Aggregate, EventSourcing, DynamoEventRepository, SnsEventNotifier, DateProvider } = require('@othree.io/chisel')
const AWS = require('aws-sdk')
const {v4: uuid} = require('uuid')
const {reducer, InitialState} = require('./reducer/club-membership-reducer')
const {ClubMembershipCommandHandler} = require('./commands/club-membership-command-handler')
const {Configuration} = require('./configuration')
const {ResponseHandler} = require('utils').websockets
const {SqsCommandHandler} = require('utils').lambda

const documentClient = new AWS.DynamoDB.DocumentClient()
const eventRepository = DynamoEventRepository(
    documentClient,
    Configuration.ClubMembershipEventsTable
)
const snsClient = new AWS.SNS()
const eventNotifier = SnsEventNotifier(Configuration.EventsTopic, 'club-membership', snsClient, true)

const eventSourcing = EventSourcing(
    eventRepository,
    reducer,
    eventNotifier,
    uuid,
    DateProvider,
    {...InitialState}
)
const apiGatewayManagementApiBuilder = (endpoint) => {
    return new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: endpoint
    })
}
const responseHandler = ResponseHandler(apiGatewayManagementApiBuilder)

const commandHandler = ClubMembershipCommandHandler(uuid)

const clubMembershipAggregate = Aggregate(
    eventSourcing,
    commandHandler.handle
)

const clubMembershipSQSHandlers = SqsCommandHandler(clubMembershipAggregate, responseHandler)

module.exports = {
    sqs: {
        ...clubMembershipSQSHandlers
    }
}