const EVENT_TYPES = Object.freeze({
    STUDENT_ENROLLED: 'StudentEnrolled',
    GRADED_ASSIGNMENT_ADDED: 'GradedAssignmentAdded',
    GRADED_ASSIGNMENT_REMOVED: 'GradedAssignmentRemoved'
})

module.exports = {EVENT_TYPES}