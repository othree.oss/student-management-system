const {EVENT_TYPES} = require('../../shared/event-types')
const {Optional} = require('@othree.io/optional')

function ClubMembershipService(clubMembershipRepository) {
    return Object.freeze({
        updateProjection: async (event, state) => {
            switch(event.type) {
                case EVENT_TYPES.STUDENT_ENROLLED:
                case EVENT_TYPES.GRADED_ASSIGNMENT_REMOVED:
                case EVENT_TYPES.GRADED_ASSIGNMENT_ADDED:
                    return clubMembershipRepository.upsert(state)
                default:
                    return Optional()
            }
        },
        getByClubAndStudent: async (clubId, studentId) => {
            return clubMembershipRepository.getByClubAndStudent(clubId, studentId)
        }
    })
}

module.exports = {ClubMembershipService}
