const Configuration = Object.freeze({
    ProjectionTable: process.env.CLUB_MEMBERSHIP_PROJECTION_TABLE,
    ClubMembershipClubStudentIndex: process.env.CLUB_MEMBERSHIP_CLUB_STUDENT_INDEX
})

module.exports = {Configuration}
