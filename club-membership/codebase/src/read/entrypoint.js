const AWS = require('aws-sdk')
const {Configuration} = require('./configuration')
const {ClubMembershipRepository} = require('./repositories/club-membership-repository')
const {ClubMembershipService} = require('./services/club-membership-service')
const {ClubMembershipHandlers} = require('./handlers/club-membership-handlers')
const {SqsUpdateProjectionHandler, BaseProjectionRestHandlers} = require('utils').lambda
const {BaseProjectionRepository} = require('utils').dynamodb
const {BaseProjectionService} = require('utils').services

const documentClient = new AWS.DynamoDB.DocumentClient()

const clubMembershipRepository = {
    ...BaseProjectionRepository(documentClient, Configuration),
    ...ClubMembershipRepository(Configuration, documentClient)
}
const clubMembershipService = {
    ...ClubMembershipService(clubMembershipRepository),
    ...BaseProjectionService(clubMembershipRepository)
}
const clubMembershipSqsHandlers = SqsUpdateProjectionHandler(clubMembershipService)
const clubMembershipRestHandlers = {
    ...BaseProjectionRestHandlers(clubMembershipService),
    ...ClubMembershipHandlers(clubMembershipService)
}

module.exports = {
    rest: {
        ...clubMembershipRestHandlers
    },
    sqs: {
        ...clubMembershipSqsHandlers
    }
}
