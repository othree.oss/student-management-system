const {createResponse} = require('utils').rest

function ClubMembershipHandlers(clubMembershipService) {
    return Object.freeze({
        getByClubAndStudentHandler: async (event) => {
            console.log('Getting entity', event)
            const {id, studentId} = event.pathParameters

            const maybeMembership = await clubMembershipService.getByClubAndStudent(id, studentId)

            if (maybeMembership.isEmpty) {
                console.log('Failed to get club membership by club', id, 'and student', studentId, maybeMembership.getError())
            }

            return maybeMembership.map(membership => createResponse(200, membership))
                .orElse(createResponse(404))
        }
    })
}

module.exports = {ClubMembershipHandlers}
