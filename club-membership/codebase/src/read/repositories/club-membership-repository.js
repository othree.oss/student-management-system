const {Try} = require('@othree.io/optional')

function ClubMembershipRepository(configuration, documentClient) {
    return Object.freeze({
        getByClubAndStudent: async (clubId, studentId) => {
            console.log('Getting club membership by club', clubId, 'and student', studentId)
            return Try(async () => {
                const result = await documentClient.query({
                    TableName: configuration.ProjectionTable,
                        IndexName: configuration.ClubMembershipClubStudentIndex,
                    KeyConditionExpression: '#clubId = :clubId and #studentId = :studentId',
                    ExpressionAttributeNames: {
                        '#clubId': 'clubId',
                        '#studentId': 'studentId'
                    },
                    ExpressionAttributeValues: {
                        ':clubId': clubId,
                        ':studentId': studentId
                    }
                }).promise()

                console.log('Query result', result)
                if (result.Count > 0) {
                    return result.Items[0]
                }
            })
        }
    })
}

module.exports = {ClubMembershipRepository}
