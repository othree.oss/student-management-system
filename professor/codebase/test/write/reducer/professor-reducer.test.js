const {InitialState, reducer} = require('../../../src/write/reducer/professor-reducer')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('InitialState', () => {
    it('should be correctly initialized', () => {
        expect(InitialState).toStrictEqual({
            rating: {
                overallRating: 0,
                stars: [0, 0, 0, 0, 0]
            }
        })
    })
})

describe('reducer', () => {
    it('should handle a PROFESSOR_CREATED event', () => {
        const event = {
            contextId: '1337',
            type: EVENT_TYPES.PROFESSOR_CREATED,
            body: {
                name: 'Chuck Norris'
            }
        }

        const state = reducer(InitialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            name: 'Chuck Norris',
            rating: {
                overallRating: 0,
                stars: [0, 0, 0, 0, 0]
            }
        })
    })

    it('should handle a RATING_POSTED event', () => {
        const event = {
            contextId: '1337',
            type: EVENT_TYPES.RATING_POSTED,
            body: {
                rating: 5
            }
        }

        const initialState = {
            id: '1337',
            name: 'Chuck Norris',
            rating: {
                overallRating: 2.73,
                stars: [5, 10, 8, 9, 1]
            }
        }

        const state = reducer(initialState, event)

        expect(state).toStrictEqual({
            id: '1337',
            name: 'Chuck Norris',
            rating: {
                overallRating: 2.8,
                stars: [5, 10, 8, 9, 2]
            }
        })
    })

    it('should not mutate the state for unhandled event types', () => {
        const event = {
            contextId: 'abc',
            type: 'UNEXPECTED_EVENT',
            body: {}
        }

        const initialState = {
            id: '1337',
            name: 'Chuck Norris',
            rating: {
                overallRating: 2.73,
                stars: [5, 10, 8, 9, 1]
            }
        }

        const state = reducer(initialState, event)

        expect(state).toEqual(initialState)
    })
})