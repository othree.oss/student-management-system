describe('Configuration', () => {
    process.env.PROFESSOR_EVENTS_TABLE = 'aws:arn:ProfessorEventsTable'
    process.env.PROFESSOR_EVENTS_TOPIC = 'aws:arn:ProfessorEventsTopic'

    const {Configuration} = require('../../src/write/configuration')

    it('should return the configured EventsTopic', () => {
        expect(Configuration.EventsTopic).toEqual('aws:arn:ProfessorEventsTopic')
    })

    it('should return the configured ProfessorEventsTable', () => {
        expect(Configuration.ProfessorEventsTable).toEqual('aws:arn:ProfessorEventsTable')
    })
})
