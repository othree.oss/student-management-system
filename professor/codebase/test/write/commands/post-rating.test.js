const {ProfessorCommandHandler} = require('../../../src/write/commands/professor-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('PostRating', () => {
    it('should return a RATING_POSTED event', async () => {

        const handler = ProfessorCommandHandler()

        const command = {
            contextId: '1337',
            type: COMMAND_TYPES.POST_RATING,
            body: {
                rating: 5
            }
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.RATING_POSTED,
            body: {
                rating: 5
            }
        })
    })
})
