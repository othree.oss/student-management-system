const {ProfessorCommandHandler} = require('../../../src/write/commands/professor-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('CreateProfessor', () => {

    const uuid = () => '1337'

    it('should return a PROFESSOR_CREATED event', async () => {
        const handler = ProfessorCommandHandler(uuid)

        const command = {
            type: COMMAND_TYPES.CREATE_PROFESSOR,
            body: {
                name: 'Chuck Norris'
            }
        }
        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '1337',
            type: EVENT_TYPES.PROFESSOR_CREATED,
            body: {
                name: 'Chuck Norris'
            }
        })
    })
})
