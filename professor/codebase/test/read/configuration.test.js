describe('Configuration', () => {
    process.env.PROFESSOR_PROJECTION_TABLE = 'aws:arn:ProfessorProjectionTable'

    const {Configuration} = require('../../src/read/configuration')

    it('should return the configured ProjectionTable', () => {
        expect(Configuration.ProjectionTable).toEqual('aws:arn:ProfessorProjectionTable')
    })
})
