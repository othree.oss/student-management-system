const {ProfessorService} = require('../../../src/read/services/professor-service')
const {EVENT_TYPES} = require('../../../src/shared/event-types')
const {Optional} = require('@othree.io/optional')

describe('ProfessorService', () => {
    describe('updateProjection', () => {

        const updateEvents = [EVENT_TYPES.PROFESSOR_CREATED, EVENT_TYPES.RATING_POSTED]

        updateEvents.forEach(eventType => {
            it(`should update the projection when the event is ${eventType}`, async () => {
                const state = {
                    id: '1337',
                    name: 'Chuck Norris',
                    rating: {
                        overallRating: 0,
                        stars: [0, 0, 0, 0, 0]
                    }
                }

                const repository = {
                    upsert: async (professor) => {
                        expect(professor).toStrictEqual(state)

                        return Optional({...professor})
                    }
                }

                const service = ProfessorService(repository)

                const maybeProfessor = await service.updateProjection({type: eventType}, state)

                expect(maybeProfessor.isPresent).toBeTruthy()
                expect(maybeProfessor.get()).toStrictEqual(state)
            })
        })

        it('should return an empty optional if the event is unhandled', async () => {
            const state = {
                id: 'abc'
            }

            const service = ProfessorService({})

            const event = {
                type: 'UNEXPECTED'
            }

            const maybeProfessor = await service.updateProjection(event, state)

            expect(maybeProfessor.isPresent).toBeFalsy()
        })

    })
})