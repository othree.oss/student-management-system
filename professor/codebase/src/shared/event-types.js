const EVENT_TYPES = Object.freeze({
    PROFESSOR_CREATED: 'ProfessorCreated',
    RATING_POSTED: 'RatingPosted'
})

module.exports = {EVENT_TYPES}
