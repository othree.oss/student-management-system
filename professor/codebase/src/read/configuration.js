const Configuration = Object.freeze({
    ProjectionTable: process.env.PROFESSOR_PROJECTION_TABLE
})

module.exports = {Configuration}
