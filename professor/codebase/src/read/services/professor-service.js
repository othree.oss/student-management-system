const {EVENT_TYPES} = require('../../shared/event-types')
const {Optional} = require('@othree.io/optional')

function ProfessorService(professorRepository) {
    return Object.freeze({
        updateProjection: async (event, state) => {
            switch(event.type) {
                case EVENT_TYPES.RATING_POSTED:
                case EVENT_TYPES.PROFESSOR_CREATED:
                    return professorRepository.upsert(state)
                default:
                    return Optional()
            }
        }
    })
}

module.exports = {ProfessorService}
