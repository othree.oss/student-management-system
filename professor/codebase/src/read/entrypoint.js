const AWS = require('aws-sdk')
const {Configuration} = require('./configuration')
const {ProfessorService} = require('./services/professor-service')
const {SqsUpdateProjectionHandler, BaseProjectionRestHandlers} = require('utils').lambda
const {BaseProjectionRepository} = require('utils').dynamodb
const {BaseProjectionService} = require('utils').services

const documentClient = new AWS.DynamoDB.DocumentClient()

const professorRepository = BaseProjectionRepository(documentClient, Configuration)
const professorService = {
    ...ProfessorService(professorRepository),
    ...BaseProjectionService(professorRepository)
}
const professorUpdateProjectionHandler = SqsUpdateProjectionHandler(professorService)
const professorRestHandlers = BaseProjectionRestHandlers(professorService)

module.exports = {
    rest: {
        ...professorRestHandlers
    },
    sqs: {
        ...professorUpdateProjectionHandler
    }
}