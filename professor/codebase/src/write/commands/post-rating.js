const {createEvent} = require('@othree.io/chisel')
const {EVENT_TYPES} = require('../../shared/event-types')

function PostRating() {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Posting rating', command, 'Professor', state)
            const {rating} = command.body

            return createEvent(
                command.contextId,
                EVENT_TYPES.RATING_POSTED,
                {
                    rating
                }
            )
        }
    })
}

module.exports = {PostRating}