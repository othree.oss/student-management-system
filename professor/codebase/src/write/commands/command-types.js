const COMMAND_TYPES = Object.freeze({
    CREATE_PROFESSOR: 'CreateProfessor',
    POST_RATING: 'PostRating'
})

module.exports = {COMMAND_TYPES}
