const {createEvent} = require('@othree.io/chisel')
const {EVENT_TYPES} = require('../../shared/event-types')

function CreateProfessor(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Creating professor', command)

            const {body: professor} = command

            const professorId = uuid()

            return createEvent(
                professorId,
                EVENT_TYPES.PROFESSOR_CREATED,
                {
                    name: professor.name
                }
            )
        }
    })
}

module.exports = {CreateProfessor}