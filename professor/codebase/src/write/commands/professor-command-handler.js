const {COMMAND_TYPES} = require('./command-types')
const {CreateProfessor} = require('./create-professor')
const {PostRating} = require('./post-rating')

function ProfessorCommandHandler(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Handling command', command, 'Current state', state)
            switch (command.type) {
                case COMMAND_TYPES.CREATE_PROFESSOR:
                    const createProfessor = CreateProfessor(uuid)
                    return createProfessor.handle(state, command)
                case COMMAND_TYPES.POST_RATING:
                    const postRating = PostRating()
                    return postRating.handle(state, command)
            }
        }
    })
}

module.exports = {ProfessorCommandHandler}
