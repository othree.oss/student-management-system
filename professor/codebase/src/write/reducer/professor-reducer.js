const {EVENT_TYPES} = require('../../shared/event-types')
const currency = require('currency.js')

const InitialState = {
    rating: {
        overallRating: 0,
        stars: [0, 0, 0, 0, 0]
    }
}

const reducer = (state, event) => {
    switch (event.type) {
        case EVENT_TYPES.PROFESSOR_CREATED:
            return professorCreated(state, event)
        case EVENT_TYPES.RATING_POSTED:
            return ratingPosted(state, event)
        default:
            return state
    }
}

const professorCreated = (state, event) => {
    const {body: professor} = event
    return {
        ...state,
        id: event.contextId,
        name: professor.name
    }
}

const ratingPosted = (state, event) => {
    const {rating} = event.body

    const newStars = state.rating.stars.map((count, index) => {
        return index === (rating - 1) ? count + 1 : count
    })

    const ratingTotals = newStars.reduce((acum, count, index) => {
        return {
            total: acum.total + (count * (index + 1)),
            totalStars: acum.totalStars + count
        }
    }, {
        total: 0,
        totalStars: 0
    })

    return {
        ...state,
        rating: {
            stars: newStars,
            overallRating: currency(ratingTotals.total / ratingTotals.totalStars, {precision: 1}).value
        }
    }
}

module.exports = {InitialState, reducer}
