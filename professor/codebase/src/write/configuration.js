const Configuration = Object.freeze({
    ProfessorEventsTable: process.env.PROFESSOR_EVENTS_TABLE,
    EventsTopic: process.env.PROFESSOR_EVENTS_TOPIC
})

module.exports = {Configuration}
