const {Aggregate, EventSourcing, DynamoEventRepository, SnsEventNotifier, DateProvider } = require('@othree.io/chisel')
const AWS = require('aws-sdk')
const {v4: uuid} = require('uuid')
const {reducer, InitialState} = require('./reducer/professor-reducer')
const {ProfessorCommandHandler} = require('./commands/professor-command-handler')
const {Configuration} = require('./configuration')
const {ResponseHandler} = require('utils').websockets
const {SqsCommandHandler} = require('utils').lambda

const documentClient = new AWS.DynamoDB.DocumentClient()
const eventRepository = DynamoEventRepository(
    documentClient,
    Configuration.ProfessorEventsTable
)
const snsClient = new AWS.SNS()
const eventNotifier = SnsEventNotifier(Configuration.EventsTopic, 'professor', snsClient, true)

const eventSourcing = EventSourcing(
    eventRepository,
    reducer,
    eventNotifier,
    uuid,
    DateProvider,
    {...InitialState}
)
const apiGatewayManagementApiBuilder = (endpoint) => {
    return new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: endpoint
    })
}
const responseHandler = ResponseHandler(apiGatewayManagementApiBuilder)

const commandHandler = ProfessorCommandHandler(uuid)

const professorAggregate = Aggregate(
    eventSourcing,
    commandHandler.handle
)

const professorSQSHandlers = SqsCommandHandler(professorAggregate, responseHandler)

module.exports = {
    sqs: {
        ...professorSQSHandlers
    }
}