#!/usr/bin/env node
const cdk = require('@aws-cdk/core')
const {ProfessorCoreStack} = require('../lib/professor-core-stack')
const {ProfessorWriteStack} = require('../lib/professor-write-stack')
const {ProfessorReadStack} = require('../lib/professor-read-stack')

const app = new cdk.App();
const env = app.node.tryGetContext('env')

new ProfessorCoreStack(env, app, `Professor-Core-Stack-${env}`)
new ProfessorWriteStack(env, app, `Professor-Write-Stack-${env}`)
new ProfessorReadStack(env, app, `Professor-Read-Stack-${env}`)
