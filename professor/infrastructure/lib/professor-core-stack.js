const cdk = require('@aws-cdk/core');
const sns = require('@aws-cdk/aws-sns');

class ProfessorCoreStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props);

        const topic = new sns.Topic(this, `Professor-Topic-${env}`, {
            topicName: `Professor-Topic-${env}`,
            displayName: 'Professor Topic',
            fifo: true,
            contentBasedDeduplication: true
        })

        new cdk.CfnOutput(this, `Output-Professor-Topic-${env}`, {
            value: topic.topicArn,
            exportName: `Professor-Topic-${env}`
        })
    }
}

module.exports = {ProfessorCoreStack}
