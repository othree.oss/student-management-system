const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const subs = require('@aws-cdk/aws-sns-subscriptions')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const iam = require('@aws-cdk/aws-iam')

class ProfessorReadStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const professorTopicARN = cdk.Fn.importValue(`Professor-Topic-${env}`)
        const professorTopic = sns.Topic.fromTopicArn(this, `Professor-Topic-${env}`, professorTopicARN)

        const professorDLQ = new sqs.Queue(this, `Professor-SQS-EventsDLQ-${env}`, {
            queueName: `Professor-SQS-EventsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const professorEventsQ = new sqs.Queue(this, `Professor-SQS-Events-${env}`, {
            queueName: `Professor-SQS-Events-${env}.fifo`,
            deadLetterQueue: {
                queue: professorDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        professorTopic.addSubscription(
            new subs.SqsSubscription(
                professorEventsQ,
                {
                    filterPolicy: {
                        bc: sns.SubscriptionFilter.stringFilter({
                            whitelist: ['professor']
                        })
                    }
                }
            )
        )

        const professorProjectionTable = new dynamodb.Table(this, `Professor-Projection-${env}`, {
            tableName: `Professor-Projection-${env}`,
            partitionKey: {name: 'id', type: dynamodb.AttributeType.STRING},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        })

        const updateProjectionHandler = new lambda.Function(this, `Professor-SqsFn-UpdateProjectionHandler-${env}`, {
            functionName: `Professor-SqsFn-UpdateProjectionHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.sqs.updateProjectionHandler',
            environment: {
                PROFESSOR_PROJECTION_TABLE: professorProjectionTable.tableName
            }
        })
        professorProjectionTable.grantReadWriteData(updateProjectionHandler)

        updateProjectionHandler.addEventSource(
            new SqsEventSource(
                professorEventsQ
            )
        )

        const getProfessorHandler = new lambda.Function(this, `Professor-RestFn-GetProfessorHandler-${env}`, {
            functionName: `Professor-RestFn-GetProfessorHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getHandler',
            environment: {
                PROFESSOR_PROJECTION_TABLE: professorProjectionTable.tableName
            }
        })
        professorProjectionTable.grantReadData(getProfessorHandler)
        getProfessorHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        const getAllProfessorsHandler = new lambda.Function(this, `Professor-RestFn-GetAllProfessorsHandler-${env}`, {
            functionName: `Professor-RestFn-GetAllProfessorsHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getAllHandler',
            environment: {
                PROFESSOR_PROJECTION_TABLE: professorProjectionTable.tableName
            }
        })
        professorProjectionTable.grantReadData(getAllProfessorsHandler)
        getAllProfessorsHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        new cdk.CfnOutput(this, `Output-Professor-RestFn-GetProfessorHandler-${env}`, {
            exportName: `Professor-RestFn-GetProfessorHandler-${env}`,
            value: getProfessorHandler.functionArn
        })

        new cdk.CfnOutput(this, `Output-Professor-RestFn-GetAllProfessorsHandler-${env}`, {
            exportName: `Professor-RestFn-GetAllProfessorsHandler-${env}`,
            value: getAllProfessorsHandler.functionArn
        })
    }
}

module.exports = {ProfessorReadStack}
