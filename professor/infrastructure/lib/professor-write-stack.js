const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const iam = require('@aws-cdk/aws-iam')

class ProfessorWriteStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const professorTopicARN = cdk.Fn.importValue(`Professor-Topic-${env}`)
        const professorTopic = sns.Topic.fromTopicArn(this, `Professor-Topic-${env}`, professorTopicARN)

        const professorEventsTable = new dynamodb.Table(this, `Professor-Events-${env}`, {
            tableName: `Professor-Events-${env}`,
            partitionKey: {name: 'contextId', type: dynamodb.AttributeType.STRING},
            sortKey: {name: 'eventDate', type: dynamodb.AttributeType.NUMBER},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST
        })

        const professorCommandsDLQ = new sqs.Queue(this, `Professor-SQS-CommandsDLQ-${env}`, {
            queueName: `Professor-SQS-CommandsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const professorCommandsQ = new sqs.Queue(this, `Professor-SQS-Commands-${env}`, {
            queueName: `Professor-SQS-Commands-${env}.fifo`,
            deadLetterQueue: {
                queue: professorCommandsDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        const commandHandler = new lambda.Function(this, `Professor-SqsFn-CommandHandler-${env}`, {
            functionName: `Professor-SqsFn-CommandHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/write/entrypoint.sqs.commandHandler',
            environment: {
                PROFESSOR_EVENTS_TABLE: professorEventsTable.tableName,
                PROFESSOR_EVENTS_TOPIC: professorTopicARN
            }
        })
        professorEventsTable.grantReadWriteData(commandHandler)
        professorTopic.grantPublish(commandHandler)

        commandHandler.addEventSource(
            new SqsEventSource(
                professorCommandsQ
            )
        )

        commandHandler.addToRolePolicy(new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: ['execute-api:Invoke', 'execute-api:ManageConnections'],
            resources: ['arn:aws:execute-api:*:*:*']
        }))

        new cdk.CfnOutput(this, `Export-Professor-SQS-Commands-${env}`, {
            exportName: `Professor-SQS-Commands-${env}`,
            value: professorCommandsQ.queueArn
        })

        new cdk.CfnOutput(this, `Export-Professor-SqsFn-CommandHandler-${env}`, {
            exportName: `Professor-SqsFn-CommandHandler-${env}`,
            value: commandHandler.functionArn
        })
    }
}

module.exports = {ProfessorWriteStack}
