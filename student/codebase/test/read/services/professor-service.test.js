const {StudentService} = require('../../../src/read/services/student-service')
const {EVENT_TYPES} = require('../../../src/shared/event-types')
const {Optional} = require('@othree.io/optional')

describe('StudentService', () => {
    describe('updateProjection', () => {

        const updateEvents = [EVENT_TYPES.STUDENT_CREATED]

        updateEvents.forEach(eventType => {
            it(`should update the projection when the event is ${eventType}`, async () => {
                const state = {
                    id: '7331',
                    name: 'Bruce Lee'
                }

                const repository = {
                    upsert: async (student) => {
                        expect(student).toStrictEqual(state)

                        return Optional({...student})
                    }
                }

                const service = StudentService(repository)

                const maybeStudent = await service.updateProjection({type: eventType}, state)

                expect(maybeStudent.isPresent).toBeTruthy()
                expect(maybeStudent.get()).toStrictEqual(state)
            })
        })

        it('should return an empty optional if the event is unhandled', async () => {
            const state = {
                id: 'abc'
            }

            const service = StudentService({})

            const event = {
                type: 'UNEXPECTED'
            }

            const maybeProfessor = await service.updateProjection(event, state)

            expect(maybeProfessor.isPresent).toBeFalsy()
        })

    })
})