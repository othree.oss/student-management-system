describe('Configuration', () => {
    process.env.STUDENT_PROJECTION_TABLE = 'aws:arn:StudentProjectionTable'

    const {Configuration} = require('../../src/read/configuration')

    it('should return the configured ProjectionTable', () => {
        expect(Configuration.ProjectionTable).toEqual('aws:arn:StudentProjectionTable')
    })
})
