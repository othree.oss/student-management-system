const {reducer, InitialState} = require('../../../src/write/reducer/student-reducer')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('InitialState', () => {
    it('should be initialized correctly', () => {
        expect(InitialState).toStrictEqual({})
    })
})

describe('reducer', () => {
    it('should handle a STUDENT_CREATED event', () => {
        const event = {
            contextId: '7331',
            type: EVENT_TYPES.STUDENT_CREATED,
            body: {
                name: 'Bruce Lee'
            }
        }

        const state = reducer(InitialState, event)

        expect(state).toStrictEqual({
            id: '7331',
            name: 'Bruce Lee'
        })
    })
})
