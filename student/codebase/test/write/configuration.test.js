describe('Configuration', () => {
    process.env.STUDENT_EVENTS_TABLE = 'aws:arn:StudentEventsTable'
    process.env.STUDENT_EVENTS_TOPIC = 'aws:arn:StudentEventsTopic'

    const {Configuration} = require('../../src/write/configuration')

    it('should return the configured EventsTopic', () => {
        expect(Configuration.EventsTopic).toEqual('aws:arn:StudentEventsTopic')
    })

    it('should return the configured StudentEventsTable', () => {
        expect(Configuration.StudentEventsTable).toEqual('aws:arn:StudentEventsTable')
    })
})
