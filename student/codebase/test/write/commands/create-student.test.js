const {StudentCommandHandler} = require('../../../src/write/commands/student-command-handler')
const {COMMAND_TYPES} = require('../../../src/write/commands/command-types')
const {EVENT_TYPES} = require('../../../src/shared/event-types')

describe('StudentCommandHandler', () => {
    const uuid = () => '7331'

    it('should return a STUDENT_CREATED event', async () => {
        const handler = StudentCommandHandler(uuid)

        const command = {
            type: COMMAND_TYPES.CREATE_STUDENT,
            body: {
                name: 'Bruce Lee'
            }
        }

        const event = await handler.handle({}, command)

        expect(event).toStrictEqual({
            contextId: '7331',
            type: EVENT_TYPES.STUDENT_CREATED,
            body: {
                name: 'Bruce Lee'
            }
        })
    })
})