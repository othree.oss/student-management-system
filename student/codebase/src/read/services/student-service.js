const {EVENT_TYPES} = require('../../shared/event-types')
const {Optional} = require('@othree.io/optional')

function StudentService(studentRepository) {
    return Object.freeze({
        updateProjection: async (event, state) => {
            switch(event.type) {
                case EVENT_TYPES.STUDENT_CREATED:
                    return studentRepository.upsert(state)
                default:
                    return Optional()
            }
        }
    })
}

module.exports = {StudentService}
