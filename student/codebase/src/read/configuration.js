const Configuration = Object.freeze({
    ProjectionTable: process.env.STUDENT_PROJECTION_TABLE
})

module.exports = {Configuration}
