const AWS = require('aws-sdk')
const {Configuration} = require('./configuration')
const {StudentService} = require('./services/student-service')
const {SqsUpdateProjectionHandler, BaseProjectionRestHandlers} = require('utils').lambda
const {BaseProjectionRepository} = require('utils').dynamodb
const {BaseProjectionService} = require('utils').services

const documentClient = new AWS.DynamoDB.DocumentClient()

const studentRepository = BaseProjectionRepository(documentClient, Configuration)
const studentService = {
    ...StudentService(studentRepository),
    ...BaseProjectionService(studentRepository)
}
const studentUpdateProjectionHandler = SqsUpdateProjectionHandler(studentService)
const studentRestHandlers = BaseProjectionRestHandlers(studentService)

module.exports = {
    rest: {
        ...studentRestHandlers
    },
    sqs: {
        ...studentUpdateProjectionHandler
    }
}
