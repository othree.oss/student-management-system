const EVENT_TYPES = Object.freeze({
    STUDENT_CREATED: 'StudentCreated'
})

module.exports = {EVENT_TYPES}
