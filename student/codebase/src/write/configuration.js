const Configuration = Object.freeze({
    StudentEventsTable: process.env.STUDENT_EVENTS_TABLE,
    EventsTopic: process.env.STUDENT_EVENTS_TOPIC
})

module.exports = {Configuration}
