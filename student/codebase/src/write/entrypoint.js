const {Aggregate, EventSourcing, DynamoEventRepository, SnsEventNotifier, DateProvider } = require('@othree.io/chisel')
const AWS = require('aws-sdk')
const {v4: uuid} = require('uuid')
const {reducer, InitialState} = require('./reducer/student-reducer')
const {StudentCommandHandler} = require('./commands/student-command-handler')
const {Configuration} = require('./configuration')
const {ResponseHandler} = require('utils').websockets
const {SqsCommandHandler} = require('utils').lambda

const documentClient = new AWS.DynamoDB.DocumentClient()
const eventRepository = DynamoEventRepository(
    documentClient,
    Configuration.StudentEventsTable
)
const snsClient = new AWS.SNS()
const eventNotifier = SnsEventNotifier(Configuration.EventsTopic, 'student', snsClient, true)

const eventSourcing = EventSourcing(
    eventRepository,
    reducer,
    eventNotifier,
    uuid,
    DateProvider,
    {...InitialState}
)
const apiGatewayManagementApiBuilder = (endpoint) => {
    return new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: endpoint
    })
}
const responseHandler = ResponseHandler(apiGatewayManagementApiBuilder)

const commandHandler = StudentCommandHandler(uuid)

const studentAggregate = Aggregate(
    eventSourcing,
    commandHandler.handle
)

const studentSQSHandlers = SqsCommandHandler(studentAggregate, responseHandler)

module.exports = {
    sqs: {
        ...studentSQSHandlers
    }
}