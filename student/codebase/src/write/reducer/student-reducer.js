const {EVENT_TYPES} = require('../../../src/shared/event-types')

const InitialState = {}

const reducer = (state, event) => {
    switch(event.type) {
        case EVENT_TYPES.STUDENT_CREATED:
            return {
                id: event.contextId,
                name: event.body.name
            }
        default:
            state
    }
}

module.exports = {InitialState, reducer}
