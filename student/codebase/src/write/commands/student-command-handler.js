const {COMMAND_TYPES} = require('./command-types')
const {CreateStudent} = require('./create-student')

function StudentCommandHandler(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Handling command', command, 'Current state', state)
            switch (command.type) {
                case COMMAND_TYPES.CREATE_STUDENT:
                    const createStudent = CreateStudent(uuid)
                    return createStudent.handle(state, command)
            }
        }
    })
}

module.exports = {StudentCommandHandler}
