const COMMAND_TYPES = Object.freeze({
    CREATE_STUDENT: 'CreateStudent'
})

module.exports = {COMMAND_TYPES}
