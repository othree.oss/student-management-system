const {createEvent} = require('@othree.io/chisel')
const {EVENT_TYPES} = require('../../shared/event-types')

function CreateStudent(uuid) {
    return Object.freeze({
        handle: async (state, command) => {
            console.log('Creating student', command)

            const {body: student} = command

            const studentId = uuid()

            return createEvent(
                studentId,
                EVENT_TYPES.STUDENT_CREATED,
                {
                    name: student.name
                }
            )
        }
    })
}

module.exports = {CreateStudent}
