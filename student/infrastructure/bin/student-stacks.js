#!/usr/bin/env node
const cdk = require('@aws-cdk/core')
const {StudentCoreStack} = require('../lib/student-core-stack')
const {StudentWriteStack} = require('../lib/student-write-stack')
const {StudentReadStack} = require('../lib/student-read-stack')

const app = new cdk.App();
const env = app.node.tryGetContext('env')

new StudentCoreStack(env, app, `Student-Core-Stack-${env}`)
new StudentWriteStack(env, app, `Student-Write-Stack-${env}`)
new StudentReadStack(env, app, `Student-Read-Stack-${env}`)
