const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const iam = require('@aws-cdk/aws-iam')

class StudentWriteStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const studentTopicARN = cdk.Fn.importValue(`Student-Topic-${env}`)
        const studentTopic = sns.Topic.fromTopicArn(this, `Student-Topic-${env}`, studentTopicARN)

        const studentEventsTable = new dynamodb.Table(this, `Student-Events-${env}`, {
            tableName: `Student-Events-${env}`,
            partitionKey: {name: 'contextId', type: dynamodb.AttributeType.STRING},
            sortKey: {name: 'eventDate', type: dynamodb.AttributeType.NUMBER},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST
        })

        const studentCommandsDLQ = new sqs.Queue(this, `Student-SQS-CommandsDLQ-${env}`, {
            queueName: `Student-SQS-CommandsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const studentCommandsQ = new sqs.Queue(this, `Student-SQS-Commands-${env}`, {
            queueName: `Student-SQS-Commands-${env}.fifo`,
            deadLetterQueue: {
                queue: studentCommandsDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        const commandHandler = new lambda.Function(this, `Student-SqsFn-CommandHandler-${env}`, {
            functionName: `Student-SqsFn-CommandHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/write/entrypoint.sqs.commandHandler',
            environment: {
                STUDENT_EVENTS_TABLE: studentEventsTable.tableName,
                STUDENT_EVENTS_TOPIC: studentTopicARN
            }
        })
        studentEventsTable.grantReadWriteData(commandHandler)
        studentTopic.grantPublish(commandHandler)

        commandHandler.addEventSource(
            new SqsEventSource(
                studentCommandsQ
            )
        )

        commandHandler.addToRolePolicy(new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: ['execute-api:Invoke', 'execute-api:ManageConnections'],
            resources: ['arn:aws:execute-api:*:*:*']
        }))

        new cdk.CfnOutput(this, `Export-Student-SQS-Commands-${env}`, {
            exportName: `Student-SQS-Commands-${env}`,
            value: studentCommandsQ.queueArn
        })

        new cdk.CfnOutput(this, `Export-Student-SqsFn-CommandHandler-${env}`, {
            exportName: `Student-SqsFn-CommandHandler-${env}`,
            value: commandHandler.functionArn
        })
    }
}

module.exports = {StudentWriteStack: StudentWriteStack}
