const cdk = require('@aws-cdk/core')
const lambda = require('@aws-cdk/aws-lambda')
const sns = require('@aws-cdk/aws-sns')
const sqs = require('@aws-cdk/aws-sqs')
const subs = require('@aws-cdk/aws-sns-subscriptions')
const {SqsEventSource} = require('@aws-cdk/aws-lambda-event-sources')
const dynamodb = require('@aws-cdk/aws-dynamodb')
const iam = require('@aws-cdk/aws-iam')

class StudentReadStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props)

        const studentTopicARN = cdk.Fn.importValue(`Student-Topic-${env}`)
        const studentTopic = sns.Topic.fromTopicArn(this, `Student-Topic-${env}`, studentTopicARN)

        const studentDLQ = new sqs.Queue(this, `Student-SQS-EventsDLQ-${env}`, {
            queueName: `Student-SQS-EventsDLQ-${env}.fifo`,
            retentionPeriod: cdk.Duration.days(14),
            fifo: true,
            contentBasedDeduplication: true
        })

        const studentEventsQ = new sqs.Queue(this, `Student-SQS-Events-${env}`, {
            queueName: `Student-SQS-Events-${env}.fifo`,
            deadLetterQueue: {
                queue: studentDLQ,
                maxReceiveCount: 1
            },
            fifo: true,
            contentBasedDeduplication: true
        })

        studentTopic.addSubscription(
            new subs.SqsSubscription(
                studentEventsQ,
                {
                    filterPolicy: {
                        bc: sns.SubscriptionFilter.stringFilter({
                            whitelist: ['student']
                        })
                    }
                }
            )
        )

        const studentProjectionTable = new dynamodb.Table(this, `Student-Projection-${env}`, {
            tableName: `Student-Projection-${env}`,
            partitionKey: {name: 'id', type: dynamodb.AttributeType.STRING},
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        })

        const updateProjectionHandler = new lambda.Function(this, `Student-SqsFn-UpdateProjectionHandler-${env}`, {
            functionName: `Student-SqsFn-UpdateProjectionHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.sqs.updateProjectionHandler',
            environment: {
                STUDENT_PROJECTION_TABLE: studentProjectionTable.tableName
            }
        })
        studentProjectionTable.grantReadWriteData(updateProjectionHandler)

        updateProjectionHandler.addEventSource(
            new SqsEventSource(
                studentEventsQ
            )
        )

        const getStudentHandler = new lambda.Function(this, `Student-RestFn-GetStudentHandler-${env}`, {
            functionName: `Student-RestFn-GetStudentHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getHandler',
            environment: {
                STUDENT_PROJECTION_TABLE: studentProjectionTable.tableName
            }
        })
        studentProjectionTable.grantReadData(getStudentHandler)
        getStudentHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        const getAllStudentsHandler = new lambda.Function(this, `Student-RestFn-GetAllStudentsHandler-${env}`, {
            functionName: `Student-RestFn-GetAllStudentsHandler-${env}`,
            runtime: lambda.Runtime.NODEJS_12_X,
            code: lambda.Code.fromAsset('../codebase'),
            timeout: cdk.Duration.seconds(30),
            memorySize: 256,
            handler: 'src/read/entrypoint.rest.getAllHandler',
            environment: {
                STUDENT_PROJECTION_TABLE: studentProjectionTable.tableName
            }
        })
        studentProjectionTable.grantReadData(getAllStudentsHandler)
        getAllStudentsHandler.grantInvoke(new iam.ServicePrincipal('apigateway.amazonaws.com'))

        new cdk.CfnOutput(this, `Output-Student-RestFn-GetStudentHandler-${env}`, {
            exportName: `Student-RestFn-GetStudentHandler-${env}`,
            value: getStudentHandler.functionArn
        })

        new cdk.CfnOutput(this, `Output-Student-RestFn-GetAllStudentsHandler-${env}`, {
            exportName: `Student-RestFn-GetAllStudentsHandler-${env}`,
            value: getAllStudentsHandler.functionArn
        })
    }
}

module.exports = {StudentReadStack}
