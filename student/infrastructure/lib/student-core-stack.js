const cdk = require('@aws-cdk/core');
const sns = require('@aws-cdk/aws-sns');

class StudentCoreStack extends cdk.Stack {
    /**
     * @param {string} env
     * @param {cdk.Construct} scope
     * @param {string} id
     * @param {cdk.StackProps} props
     */
    constructor(env, scope, id, props) {
        super(scope, id, props);

        const topic = new sns.Topic(this, `Student-Topic-${env}`, {
            topicName: `Student-Topic-${env}`,
            displayName: 'Student Topic',
            fifo: true,
            contentBasedDeduplication: true
        })

        new cdk.CfnOutput(this, `Output-Student-Topic-${env}`, {
            value: topic.topicArn,
            exportName: `Student-Topic-${env}`
        })
    }
}

module.exports = {StudentCoreStack}
